<?php
$sLangName = "Französisch";
// -------------------------------
// RESOURCE IDENTIFIER = STRING
// -------------------------------
$aLang = array(
    'charset' => 'UTF-8',
    'BTN_NEWS'                                              => 'Nouveautés',
    'Newsverwaltung'                                        => 'Gestion des nouveautés',
    'ConfirmComment'                                        => 'Votre commentaire a été envoyé avec succès !',
    'AbortComment'                                          => 'Le champ des commentaires est vide.'

);