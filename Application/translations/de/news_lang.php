<?php
$sLangName = "Deutsch";
$iLangNr = 0;
$aLang = array(
    'charset'           => 'UTF-8',
    'BTN_NEWS'          => 'Neuigkeiten',
    'Newsverwaltung'    => 'Newsverwaltung',
    'ConfirmComment'    => 'Ihr Kommentar wurde erfolgreich abgeschickt!',
    'AbortComment'      => 'Das Kommentarfeld ist leer.'
);