<?php
$sLangName = "English";

$aLang = array(
    'charset'   => 'UTF-8',
    'BTN_NEWS'                                              => 'News',
    'Newsverwaltung'                                        => 'Manage news',
    'ConfirmComment'                                        => 'Your comment has been sent',
    'AbortComment'                                          => 'The comments field is empty.'

);