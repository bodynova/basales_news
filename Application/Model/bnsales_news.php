<?php

namespace Bodynova\bnSales_News\Application\Model;

use OxidEsales\Eshop\Core\Registry;
use oxRegistry;
use oxDb;
use oxField;
use oxUtilsView;

class bnsales_news extends bnsales_news_parent
{
    /**
     * Current class name
     *
     * @var string
     */
    protected $_sClassName = 'bnsales_news';

    /**
     * get long description, parsed through smarty
     *
     * @return string
     */

    public function getLongDesc()
    {

        /** @var \OxidEsales\Eshop\Core\UtilsView $oUtilsView */
        $oUtilsView = \OxidEsales\Eshop\Core\Registry::getUtilsView();
        return $oUtilsView->parseThroughSmarty($this->bnsales_news__oxlongdesc->getRawValue(), $this->getId() . $this->getLanguage(), null, true);
    }

    /**
     * get short description, parsed through smarty
     *
     * @return string
     */
    public function getShortDesc()
    {
        /** @var \OxidEsales\Eshop\Core\UtilsView $oUtilsView */

        $oUtilsView = \OxidEsales\Eshop\Core\Registry::getUtilsView();
        return $oUtilsView->parseThroughSmarty($this->bnsales_news__oxshortdesc->getRawValue(), $this->getId() . $this->getLanguage(), null, true);
    }

    public function getOldNews($oxid){
        $this->init('bnsales_news');

        return $this->load($oxid);

        $oDb = \OxidEsales\Eshop\Core\DatabaseProvider::getDb();
            // performance - only join if user is logged in
            $sSelect = "SELECT * FROM bnsales_news AS a
                  LEFT JOIN oxuser2news AS b ON a.OXID = b.OXNEWSID
                  WHERE b.OXUSERID ='" . $id . "' ORDER BY OXDATE DESC";


        $res = $oDb->getAll($sSelect);

        //echo $sSelect;

        return $res[0];
    }


}