<?php


namespace Bodynova\bnSales_News\Application\Model;


use OxidEsales\Eshop\Core\DatabaseProvider;

class bnsales_list extends \OxidEsales\Eshop\Application\Controller\FrontendController
{
    /**
     * Current class name
     *
     * @var string
     */
    protected $_sClassName = 'bnsales_list';

    public function render(){
        parent::render();
    }

    public function getList(){
        $oUser = $this->getUser();

        $oDb = DatabaseProvider::getDb(DatabaseProvider::FETCH_MODE_ASSOC);
        $query = 'SELECT a.OXID FROM bnsales_news as a LEFT JOIN oxuser2news as b ON a.OXID = b.OXNEWSID WHERE b.OXUSERID = "'.$oUser->getId().'" ORDER BY OXDATE DESC';
        return $oDb->getAll($query);
    }
}