<?php

namespace Bodynova\bnSales_News\Application\Model;


class bnsales_article extends bnsales_article_parent
{
    public function render()
    {
        return parent::render();
    }

    public function isAktionbeendet(){

        $date = strtotime($this->oxarticles__aktionsende->value);
        $schalter = false;
        if($this->oxarticles__bnflagaktion == 1){
            $schalter = true;
        }
        if($date>=time() && $schalter){
            return true;
        } else return false;
    }
}