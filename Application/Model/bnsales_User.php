<?php
namespace Bodynova\bnSales_News\Application\Model;



use OxidEsales\Eshop\Core\DatabaseProvider;
use OxidEsales\Eshop\Core\Registry;

class bnsales_User extends bnsales_User_parent
{
    public function render()
    {
        // checks if private sales allows further tasks
        return parent::render();
    }

    public function getCountUnreadMessages($lang = null){

        $arrUser = array($this->oxuser__oxid->value);
        if($lang == 0){
            $lang = '';
        } else {
            $lang = '_' . $lang;
        }
        $query = 'SELECT COUNT(*) AS Anzahl FROM bnsales_news AS a
                  LEFT JOIN oxuser2news AS b ON a.OXID = b.OXNEWSID
                  WHERE b.gelesen = 0 AND b.OXUSERID = ? AND (a.OXACTIVE' . $lang . ' = 1 OR (OXACTIVEFROM < CURRENT_TIMESTAMP AND OXACTIVETO > CURRENT_TIMESTAMP))';
        try{
            $oDb = DatabaseProvider::getDb(DatabaseProvider::FETCH_MODE_NUM);
            return $result = $oDb->getOne($query,$arrUser);

            //$this->_aViewData['ungelesen'] = $result[0];
            print_r($result[0][0]);
            die();
        } catch(\Exception $e){
            echo $e->getMessage() , "\n";
        }
    }


    public function login($sUser, $sPassword, $blCookie = false)
    {
        if ($this->isAdmin() && !count(Registry::getUtilsServer()->getOxCookie())) {
            /** @var \OxidEsales\Eshop\Core\Exception\CookieException $oEx */
            $oEx = oxNew(\OxidEsales\Eshop\Core\Exception\CookieException::class);
            $oEx->setMessage('ERROR_MESSAGE_COOKIE_NOCOOKIE');
            throw $oEx;
        }

        $oConfig = $this->getConfig();


        if ($sPassword) {
            $sShopID = $oConfig->getShopId();
            $this->_dbLogin($sUser, $sPassword, $sShopID);
        }

        $this->onLogin($sUser, $sPassword);

        //login successful?
        if ($this->oxuser__oxid->value) {
            // yes, successful login

            //resetting active user
            $this->setUser(null);

            if ($this->isAdmin()) {
                Registry::getSession()->setVariable('auth', $this->oxuser__oxid->value);
            } else {
                Registry::getSession()->setVariable('usr', $this->oxuser__oxid->value);
            }

            // cookie must be set ?
            if ($blCookie && $oConfig->getConfigParam('blShowRememberMe')) {
                Registry::getUtilsServer()->setUserCookie($this->oxuser__oxusername->value, $this->oxuser__oxpassword->value, $oConfig->getShopId(), 31536000, $this->oxuser__oxpasssalt->value);
            }

            return true;
        } else {
            /** @var \OxidEsales\Eshop\Core\Exception\UserException $oEx */
            $oEx = oxNew(\OxidEsales\Eshop\Core\Exception\UserException::class);
            $oEx->setMessage('ERROR_MESSAGE_USER_NOVALIDLOGIN');
            die('Falsch passwort');
            throw $oEx;
        }
    }


}