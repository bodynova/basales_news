[{include file="headitem.tpl" title="GENERAL_ADMIN_TITLE"|oxmultilangassign}]
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
<link rel="stylesheet" href="../../../modules/bodynova/bnsales_news/out/css/news.css">
<script type="text/javascript" src="../../../modules/bodynova/bnsales_news/out/js/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>

<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

<script type="text/javascript" src="../../../modules/bodynova/bnsales_news/out/js/technical_details.js"></script>
<textarea hidden id="sel"></textarea>
<div class="row">
    <div class=" col-12 col-sm-10 col-md-8 col-lg-6">
        <div class="card text-center">
            <form method="post" action="[{ $oViewConf->getSslSelfLink()}]" id="newsForm">
                <input type="hidden" name="cl" value="technicaldetails">
                <input type="hidden" name="fnc" value="NewsBackend">
                <div class="card-header">
                    <h2>Erstelle neue NEWS</h2>
                </div>
                <div class="card-body text-left">
                    [{assign var="gruppen" value=$oView->getUsergroups()}]
                    <div class="form-group">
                        <label for="gruppe">Benutzergruppen<small>(Für Mehrfachauswahl Steuerungs- bzw. Umschalttaste gedrückt halten)</small></label>
                        <select name="gruppenSelect[]" class="form-control" multiple>
                            <option selected value="Alle">Alle</option>
                            [{foreach from=$gruppen item='item'}]
                            <option value="[{$item.OXID}]">[{$item.OXTITLE}]</option>
                            [{/foreach}]
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="date">Datum</label><br>
                        <input required id="date" name="date" type="date">
                    </div>
                    <div class="form-group row">
                        <div class="col-12 col-sm-6">
                            <label for="dateFrom">Aktiv von</label><br>
                            <input required id="dateFrom" name="dateFrom" type="date">
                        </div>
                        <div class="col-12 col-sm-6">
                            <label for="dateTo">Aktiv bis</label><br>
                            <input id="dateTo" name="dateTo" type="date">
                        </div>
                    </div>
                    <div id="buttonBar">
                        <button content="" type="button" class="btn btn-default" onclick="ptag(this)" style="display:none">Abschnitt</button>
                        <button content="" type="button" class="btn btn-default" onclick="fett(this)" style="display:none"><b>Fett</b></button>
                        <button content="" type="button" class="btn btn-default" onclick="kursiv(this)" style="display:none"><i>Kursiv</i></button>
                        <button content="" type="button" class="btn btn-default" onclick="underline(this)" style="display:none"><u>Underline</u></button>
                        <button content="" type="button" class="btn btn-default idUmbruch" onclick="umbruch(this)" style="display:none">Zeilen<br>umbruch</button><br>
                    </div>
                    <div class="form-group row">
                        <div class="col-8 col-sm-6">
                            <label for="title">Titel DE</label><br>
                            <input  id="title" name="title" type="text" style="width:350px; max-width:100%">
                        </div>
                        <div class="col-4 col-sm-6">
                            <label for="selectBox">Aktiv</label><br>
                            <input onchange="activeFromTo(this,'');" id="selectBox" name="active" type="checkbox">
                            <button type="button" class="btn btn-warning float-right" onclick="showText(null,'')">Vorschau</button>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="content">Inhalt DE</label>
                        <textarea onkeydown="proofKey(event,this)" onfocusout="checkHTML($(this)[0].value,'content');" onfocusin="changeButtonContent('')" class="form-control" id="content" name="content" rows="3"></textarea>
                    </div>
                    <div class="form-group row">
                        <div class="col-8 col-sm-6">
                            <label for="title_1">Titel EN</label><br>
                            <input  id="title_1" name="title_1" type="text" style="width:350px; max-width:100%">
                        </div>
                        <div class="col-4 col-sm-6">
                            <label for="selectBox_1">Aktiv</label><br>
                            <input onchange="activeFromTo(this,'_1');" id="selectBox_1" name="active_1" type="checkbox">
                            <button type="button" class="btn btn-warning float-right" onclick="showText(null,'_1')">Vorschau</button>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="content_1">Inhalt EN</label>
                        <textarea onkeydown="proofKey(event,this)" onfocusout="checkHTML($(this)[0].value,'content_1');" onfocusin="changeButtonContent('_1')" class="form-control" id="content_1" name="content_1" rows="3"></textarea>
                    </div>
                    <div class="form-group row">
                        <div class="col-8 col-sm-6">
                            <label for="title_2">Titel FR</label><br>
                            <input  id="title_2" name="title_2" type="text" style="width:350px; max-width:100%">
                        </div>
                        <div class="col-4 col-sm-6">
                            <label for="selectBox_2">Aktiv</label><br>
                            <input onchange="activeFromTo(this,'_2');" id="selectBox_2" name="active_2" type="checkbox">
                            <button type="button" class="btn btn-warning float-right" onclick="showText(null,'_2')">Vorschau</button>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="content_2">Inhalt FR</label>
                        <textarea onkeydown="proofKey(event,this)" onfocusout="checkHTML($(this)[0].value,'content_2');" onfocusin="changeButtonContent('_2')" class="form-control" id="content_2" name="content_2" rows="3"></textarea>
                    </div>
                </div>
                <div class="card-footer text-muted">
                    <button class="btn btn-outline-success" type="submit" >Erstellen</button>
                </div>
            </form>
        </div>
    </div>
    <div class="col-12 col-sm-10 col-md-8 col-lg-6">
        [{assign var="news" value=$oView->getNews()}]
        <div class="card">
            <div class="card-header">
                <h2>Alle Neuigkeiten</h2>
                <button style="background-color:white;border-color:grey" type="button" class="btn" onclick="changeLang('de')"><span class="bn-flag-de"></span></button>
                <button style="background-color:white;border-color:grey" type="button" class="btn" onclick="changeLang('en')"><span class="bn-flag-en"></span></button>
                <button style="background-color:white;border-color:grey" type="button" class="btn" onclick="changeLang('fr')"><span class="bn-flag-fr"></span></button>
            </div>
            <div class="card-body">
                [{foreach from=$news item='newsItem'}]
                [{*TODO: bearbeiten direkt hier!*}]
                [{* deutsch*}]
                <div class="bigDE" id="BigDE[{$newsItem.OXID}]" [{if $lang ne ""}]style="display:none"[{/if}]>
                    <button onclick="editNews('[{$newsItem.OXID}]','de')" type="button" class="btn btn-default pull-right" data-toggle="tooltip" title="bearbeiten"><span class="fa fa-pencil-alt"></span></button>
                    <strong style="padding-right:10px">[{$newsItem.OXSHORTDESC}]</strong>   [{$newsItem.OXDATE|date_format:"%d.%m.%Y"}]<br>
                    <span id="despan[{$newsItem.OXID}]">[{$newsItem.OXLONGDESC}]</span>
                    <hr>
                </div>
                [{* deutsch*}]

                [{* englisch*}]
                <div class="bigEN" id="BigEN[{$newsItem.OXID}]" [{if $lang ne "_1"}]style="display:none"[{/if}]>
                    <button onclick="editNews('[{$newsItem.OXID}]','en')" type="button" class="btn btn-default pull-right" data-toggle="tooltip" title="bearbeiten"><span class="fa fa-pencil-alt"></span></button>
                    <strong style="padding-right:10px">[{$newsItem.OXSHORTDESC_1}]</strong>   [{$newsItem.OXDATE|date_format:"%d.%m.%Y"}]<br>
                    <span id="enspan[{$newsItem.OXID}]">[{$newsItem.OXLONGDESC_1}]</span>
                    <hr>
                </div>
                [{* englisch*}]

                [{* franz*}]
                <div class="bigFR" id="BigFR[{$newsItem.OXID}]" [{if $lang ne "_2"}]style="display:none"[{/if}]>
                    <button onclick="editNews('[{$newsItem.OXID}]','fr')" type="button" class="btn btn-default pull-right" data-toggle="tooltip" title="bearbeiten"><span class="fa fa-pencil-alt"></span></button>
                    <strong style="padding-right:10px">[{$newsItem.OXSHORTDESC_2}]</strong>   [{$newsItem.OXDATE|date_format:"%d.%m.%Y"}]<br>
                    <span id="frspan[{$newsItem.OXID}]">[{$newsItem.OXLONGDESC_2}]</span>
                    <hr>
                </div>
                [{* franz*}]

                [{* modal *}]
                <div class="modal" id="editNewsModal[{$newsItem.OXID}]" tabindex="-1" role="dialog">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">News Bearbeiten</h5>
                                <button style="background-color:white;border-color:grey" type="button" class="btn" onclick="changeLangModal('[{$newsItem.OXID}]','de')"><span class="bn-flag-de"></span></button>
                                <button style="background-color:white;border-color:grey" type="button" class="btn" onclick="changeLangModal('[{$newsItem.OXID}]','en')"><span class="bn-flag-en"></span></button>
                                <button style="background-color:white;border-color:grey" type="button" class="btn" onclick="changeLangModal('[{$newsItem.OXID}]','fr')"><span class="bn-flag-fr"></span></button>

                            </div>
                            <form action="[{ $oViewConf->getSslSelfLink()}]" method="post">
                                <input type="hidden" name="oxid" value="[{$newsItem.OXID}]">
                                <input type="hidden" name="fnc" value="saveNews"/>
                                <input type="hidden" name="cl" value="technicaldetails"/>
                                <input id="langInput" type="hidden" name="lang" value="[{$lang}]"/>
                                <div class="modal-body" id="modalDE[{$newsItem.OXID}]" [{if $lang ne ""}]style="display:none"[{/if}]>
                                    <label for="inputActive"><u>Aktiv:</u></label>
                                    <input type="checkbox" name="inputActive" value="[{$newsItem.OXACTIVE}]" [{if $newsItem.OXACTIVE == 1}]checked[{/if}]><br>
                                    <label for="inputShort"><u>Titel:</u></label>
                                    <input name="inputShort" type="text" value="[{$newsItem.OXSHORTDESC}]">
                                    <label for="inputDate"><u>Datum:</u></label>
                                    <input name="inputDate" type="date" value="[{$newsItem.OXDATE}]"><br>
                                    <button type="button" class="btn btn-default" onclick="ptag('[{$newsItem.OXID}]','de')">Abschnitt</button>
                                    <button type="button" class="btn btn-default" onclick="fett('[{$newsItem.OXID}]','de')"><b>Fett</b></button>
                                    <button type="button" class="btn btn-default" onclick="kursiv('[{$newsItem.OXID}]','de')"><i>Kursiv</i></button>
                                    <button type="button" class="btn btn-default" onclick="underline('[{$newsItem.OXID}]','de')"><u>Underline</u></button>
                                    <button type="button" class="btn btn-default idUmbruch" onclick="umbruch('[{$newsItem.OXID}]','de')">Zeilen<br>umbruch</button><br>
                                    <label for="inputLong"><u>Inhalt:</u></label>
                                    <textarea id="dexy[{$newsItem.OXID}]" onkeydown="proofKey(event,this)" style="width:100%"  type="text" name="inputLong" oninput='this.style.height = "";this.style.height = this.scrollHeight + "px"'>[{$newsItem.OXLONGDESC}]</textarea>
                                </div>
                                <div id="modalEN[{$newsItem.OXID}]" class="modal-body" [{if $lang ne "_1"}]style="display:none"[{/if}]>
                                    <label for="inputActive_1"><u>Aktiv:</u></label>
                                    <input type="checkbox" name="inputActive_1" value="[{$newsItem.OXACTIVE_1}]" [{if $newsItem.OXACTIVE_1 == 1}]checked[{/if}]><br>
                                    <label for="inputShort_1"><u>Titel:</u></label>
                                    <input name="inputShort_1" type="text" value="[{$newsItem.OXSHORTDESC_1}]">
                                    <label for="inputDate_1"><u>Datum:</u></label>
                                    <input name="inputDate_1" type="date" value="[{$newsItem.OXDATE}]"><br>
                                    <button type="button" class="btn btn-default" onclick="ptag('[{$newsItem.OXID}]','en')">Abschnitt</button>
                                    <button type="button" class="btn btn-default" onclick="fett('[{$newsItem.OXID}]','en')"><b>Fett</b></button>
                                    <button type="button" class="btn btn-default" onclick="kursiv('[{$newsItem.OXID}]','en')"><i>Kursiv</i></button>
                                    <button type="button" class="btn btn-default" onclick="underline('[{$newsItem.OXID}]','en')"><u>Underline</u></button>
                                    <button type="button" class="btn btn-default idUmbruch" onclick="umbruch('[{$newsItem.OXID}]','en')">Zeilen<br>umbruch</button><br>
                                    <label for="inputLong_1"><u>Inhalt:</u></label>
                                    <textarea onkeydown="proofKey(event,this)" style="width:100%" id="enxy[{$newsItem.OXID}]" type="text" name="inputLong_1" oninput='this.style.height = "";this.style.height = this.scrollHeight + "px"'>[{$newsItem.OXLONGDESC_1}]</textarea>
                                </div>
                                <div id="modalFR[{$newsItem.OXID}]" class="modal-body" [{if $lang ne "_2"}]style="display:none"[{/if}]>
                                    <label for="inputActive_2"><u>Aktiv:</u></label>
                                    <input type="checkbox" name="inputActive_2" value="[{$newsItem.OXACTIVE_2}]" [{if $newsItem.OXACTIVE_2 == 1}]checked[{/if}]><br>
                                    <label for="inputShort_2"><u>Titel:</u></label>
                                    <input name="inputShort_2" type="text" value="[{$newsItem.OXSHORTDESC_2}]">
                                    <label for="inputDate_2"><u>Datum:</u></label>
                                    <input name="inputDate_2" type="date" value="[{$newsItem.OXDATE}]"><br>
                                    <button type="button" class="btn btn-default" onclick="ptag('[{$newsItem.OXID}]','fr')">Abschnitt</button>
                                    <button type="button" class="btn btn-default" onclick="fett('[{$newsItem.OXID}]','fr')"><b>Fett</b></button>
                                    <button type="button" class="btn btn-default" onclick="kursiv('[{$newsItem.OXID}]','fr')"><i>Kursiv</i></button>
                                    <button type="button" class="btn btn-default" onclick="underline('[{$newsItem.OXID}]','fr')"><u>Underline</u></button>
                                    <button type="button" class="btn btn-default idUmbruch" onclick="umbruch('[{$newsItem.OXID}]','fr')">Zeilen<br>umbruch</button><br>
                                    <label for="inputLong_2"><u>Inhalt:</u></label>
                                    <textarea onkeydown="proofKey(event,this)" style="width:100%" id="frxy[{$newsItem.OXID}]" type="text" name="inputLong_2" oninput='this.style.height = "";this.style.height = this.scrollHeight + "px"'>[{$newsItem.OXLONGDESC_2}]</textarea>
                                </div>
                                <div class="modal-footer">
                                    <span id="checkFA" class="fa fa-check-circle fa-2x" style="color:green;display:none"></span>
                                    <button type="submit" class="btn btn-primary">Speichern</button>
                                    <button id="abortButton" type="button" lang="de" class="btn btn-secondary" data-dismiss="modal" onclick="isEmpty('[{$newsItem.OXID}]',this.lang)">Abbrechen</button>
                                    <button id="previewButton" type="button" lang="de" class="btn btn-warning float-left" onclick="showText('[{$newsItem.OXID}]',this.lang)">Vorschau</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                [{* modal *}]

                [{/foreach}]
            </div>
        </div>

    </div>
</div>

[{* modal *}]
<div class="modal fade" id="preview" tabindex="-1" role="dialog" >
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Vorschau</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="previewBody">

            </div>
            <div class="modal-footer">
                <button id="dismiss" type="button" class="btn btn-secondary" data-dismiss="modal" onclick="closeVorschau()">Close</button>
            </div>
        </div>
    </div>
</div>
[{* modal *}]


[{include file="bottomnaviitem.tpl"}]

[{include file="bottomitem.tpl"}]
[{if $oxid ne -1}]
    [{if $lang eq ""}]
        [{assign var=langCode value="de"}]
    [{elseif $lang eq "_1"}]
        [{assign var=langCode value="en"}]
    [{elseif $lang eq "_2"}]
        [{assign var=langCode value="fr"}]
    [{/if}]
    <script type="text/javascript">
        editNews('[{$oxid}]','[{$langCode}]');
        var speed = $("#[{$langCode}]span[{$oxid}]").offset().top;
        if(speed>8000){
            speed /=8;
        }else if(speed >4000){
            speed /=4;
        } else if(speed>2000){
            speed /=2;
        }
        $('.box').animate({
            scrollTop: $("#[{$langCode}]span[{$oxid}]").offset().top-400
        }, speed/2);
        $('#checkFA').show();
        setTimeout(function(){
            $('#checkFA').hide();
        },2500);


    </script>
[{/if}]