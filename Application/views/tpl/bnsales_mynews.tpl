[{assign var="bool" value=0}]
[{assign var="counter" value=0}]
[{assign var="lang" value="_"|cat:$oViewConf->getActLanguageId()}]
[{if $lang =='_0'}][{assign var="lang" value=''}][{/if}]
[{assign var="short" value="OXSHORTDESC"|cat:$lang}]
[{assign var="long" value="OXLONGDESC"|cat:$lang}]

<div class="modal fade" id="userNews">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				[{* $oxcmp_user->oxuser__oxfname->rawValue}]
				[{$oxcmp_user->oxuser__oxlname->rawValue*}]
				<b>[{$myuser[0].OXDATE|date_format:"%d.%m.%Y"}]</b>
				[{*<button id="modalClose" type="button" class="close" data-dismiss="modal">&times;</button>*}]
			</div>
			[{assign var="counter" value=0}]
			<div class="modal-body">
					<div class="card">
						<div class="card-header">
							<h3 id="headNews">
								[{$myuser[0].$short}]
							</h3>
						</div>
						<div id="bodyNews" class="card-body">
							[{$myuser[0].$long}]
							<hr>
							<form action="[{ $oViewConf->getSslSelfLink()}]" method="post" id="formEmail">
								<div class="hidden">
									[{$oViewConf->getHiddenSid()}]
									<input type="hidden" name="userEmail" value="[{$oxcmp_user->oxuser__oxusername->value}]">
									<input type="hidden" name="oxid" value="[{$myuser[0].OXID}]">
									<input type="hidden" name="fnc" value="commentEmail"/>
									<input type="hidden" name="cl" value="bnsales_mynews"/>
									<input type="hidden" name="vorname" value="[{$oxcmp_user->oxuser__oxfname->rawValue}]"/>
									<input type="hidden" name="nachname" value="[{$oxcmp_user->oxuser__oxlname->rawValue}]"/>
									<input type="hidden" name="firma" value="[{$oxcmp_user->oxuser__oxcompany->rawValue}]"/>
								</div>
								<div class="form-group">
									<label for="comment">[{oxmultilang ident="Kommentar"}]</label>
									<textarea rows="5" style="resize: none" class="form-control" id="commentEmailTA"></textarea>
								</div>
								<button type="button" class="btn btn-outline-info pull-right" onclick="commentEmail()">[{oxmultilang ident="SUBMIT"}]</button>
							</form>
						</div>
					</div>

			</div>
			<div class="modal-footer" style="display:inline-block">
				<div class="row">

					[{*<div class="col-4">	<button class="btn btn-outline-warning" style="align-items:start;" onclick="back();">Zurück</button>
					</div>*}]
					<div class="col-4 offset-4">	<a class="btn btn-outline-dark" href="[{oxgetseourl ident=$oViewConf->getSelfLink()|cat:"cl=bnsales_oldnews&fnc=show"}]">[{oxmultilang ident="ALLENEWS"}]</a>
					</div>
					<div class="col-4">	<button class="btn btn-outline-success pull-right" onclick="next('[{$myuser[0].OXID}]',[{$oViewConf->getActLanguageId()}]);">[{oxmultilang ident="NEXT"}]</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

