<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script type="text/javascript" src="../../../modules/bodynova/bnsales_news/out/js/analyze.js"></script>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous"></head>
<body>
<div class="container col-12">
    <div class="card">
        <div class="card-header text-center"><h2>Benutzeranalyse</h2></div>
        <div class="card-body">

            [{assign var="sum" value=0}]

            [{* Gerätetypen *}]
            [{assign var="WindowsPhone" value=0}]
            [{assign var="Windows" value=0}]
            [{assign var="iPhone" value=0}]
            [{assign var="iPad" value=0}]
            [{assign var="Kindle" value=0}]
            [{assign var="Android" value=0}]
            [{assign var="PlayBook" value=0}]
            [{assign var="BlackBerry" value=0}]
            [{assign var="Macintosh" value=0}]
            [{assign var="Linux" value=0}]
            [{assign var="Palm" value=0}]
            [{* Gerätetypen *}]

            [{* Browser *}]
            [{assign var="Chrome" value=0}]
            [{assign var="Firefox" value=0}]
            [{assign var="Safari" value=0}]
            [{assign var="InternetExplorer" value=0}]
            [{assign var="Opera" value=0}]
            [{assign var="BlackBerry" value=0}]
            [{assign var="Mozilla" value=0}]
            [{* Browser *}]

            [{* Kombination *}]
            [{assign var="MacSafari" value=0}]
            [{assign var="MacFirefox" value=0}]
            [{assign var="MacChrome" value=0}]

            [{assign var="WinSafari" value=0}]
            [{assign var="WinFirefox" value=0}]
            [{assign var="WinChrome" value=0}]

            [{assign var="AndroidSafari" value=0}]
            [{assign var="AndroidFirefox" value=0}]
            [{assign var="AndroidChrome" value=0}]

            [{assign var="iPhoneSafari" value=0}]
            [{assign var="iPhoneFirefox" value=0}]
            [{assign var="iPhoneChrome" value=0}]

            [{assign var="iPadSafari" value=0}]
            [{assign var="iPadFirefox" value=0}]
            [{assign var="iPadChrome" value=0}]
            [{* Kombination *}]

            [{assign var="data" value=$oView->getData()}]
            [{foreach from=$data item="key"}]
                [{assign var="key" value=$key|json_decode}]

                [{assign var="device" value=$key[0]->Geraete}]
                [{foreach from=$device item="deviceKey"}]

                    [{* Kombination *}]
                    [{if $deviceKey->Name == "Macintosh" && $deviceKey->BName == "Safari"}]
                        [{assign var="MacSafari" value=$MacSafari+$deviceKey->Aufrufe}]
                    [{elseif $deviceKey->Name == "Macintosh" && $deviceKey->BName == "Firefox"}]
                        [{assign var="MacFirefox" value=$MacFirefox+$deviceKey->Aufrufe}]
                    [{elseif $deviceKey->Name == "Macintosh" && $deviceKey->BName == "Chrome"}]
                        [{assign var="MacChrome" value=$MacChrome+$deviceKey->Aufrufe}]

                    [{elseif $deviceKey->Name == "Windows" && $deviceKey->BName == "Safari"}]
                        [{assign var="WinSafari" value=$WinSafari+$deviceKey->Aufrufe}]
                    [{elseif $deviceKey->Name == "Windows" && $deviceKey->BName == "Firefox"}]
                        [{assign var="WinFirefox" value=$WinFirefox+$deviceKey->Aufrufe}]
                    [{elseif $deviceKey->Name == "Windows" && $deviceKey->BName == "Chrome"}]
                        [{assign var="WinChrome" value=$WinChrome+$deviceKey->Aufrufe}]

                    [{elseif $deviceKey->Name == "Android" && $deviceKey->BName == "Safari"}]
                        [{assign var="AndroidSafari" value=$AndroidSafari+$deviceKey->Aufrufe}]
                    [{elseif $deviceKey->Name == "Android" && $deviceKey->BName == "Firefox"}]
                        [{assign var="AndroidFirefox" value=$AndroidFirefox+$deviceKey->Aufrufe}]
                    [{elseif $deviceKey->Name == "Android" && $deviceKey->BName == "Chrome"}]
                        [{assign var="AndroidChrome" value=$AndroidChrome+$deviceKey->Aufrufe}]

                    [{elseif $deviceKey->Name == "iPhone" && $deviceKey->BName == "Safari"}]
                        [{assign var="iPhoneSafari" value=$iPhoneSafari+$deviceKey->Aufrufe}]
                    [{elseif $deviceKey->Name == "iPhone" && $deviceKey->BName == "Firefox"}]
                        [{assign var="iPhoneFirefox" value=$iPhoneFirefox+$deviceKey->Aufrufe}]
                    [{elseif $deviceKey->Name == "iPhone" && $deviceKey->BName == "Chrome"}]
                        [{assign var="iPhoneChrome" value=$iPhoneChrome+$deviceKey->Aufrufe}]

                    [{elseif $deviceKey->Name == "iPad" && $deviceKey->BName == "Safari"}]
                        [{assign var="iPadSafari" value=$iPadSafari+$deviceKey->Aufrufe}]
                    [{elseif $deviceKey->Name == "iPad" && $deviceKey->BName == "Firefox"}]
                        [{assign var="iPadFirefox" value=$iPadFirefox+$deviceKey->Aufrufe}]
                    [{elseif $deviceKey->Name == "iPad" && $deviceKey->BName == "Chrome"}]
                        [{assign var="iPadChrome" value=$iPadChrome+$deviceKey->Aufrufe}]
                    [{/if}]
                    [{* Kombination *}]


                    [{* Browser*}]
                    [{if $deviceKey->BName == "Chrome"}]
                        [{assign var="Chrome" value=$Chrome+$deviceKey->Aufrufe}]
                    [{elseif $deviceKey->BName == "Firefox"}]
                        [{assign var="Firefox" value=$Firefox+$deviceKey->Aufrufe}]
                    [{elseif $deviceKey->BName == "Safari"}]
                        [{assign var="Safari" value=$Safari+$deviceKey->Aufrufe}]
                    [{elseif $deviceKey->BName == "InternetExplorer"}]
                        [{assign var="InternetExplorer" value=$InternetExplorer+$deviceKey->Aufrufe}]
                    [{elseif $deviceKey->BName == "Opera"}]
                        [{assign var="Opera" value=$Opera+$deviceKey->Aufrufe}]
                    [{elseif $deviceKey->BName == "BlackBerry"}]
                        [{assign var="BlackBerry" value=$BlackBerry+$deviceKey->Aufrufe}]
                    [{elseif $deviceKey->BName == "Mozilla"}]
                        [{assign var="Mozilla" value=$Mozilla+$deviceKey->Aufrufe}]
                    [{/if}]
                    [{* Browser*}]


                    [{* Gerätetypen *}]
                    [{if $deviceKey->Name == "Windows Phone"}]
                        [{assign var="WindowsPhone" value=$WindowsPhone+$deviceKey->Aufrufe}]
                    [{elseif $deviceKey->Name == "Windows"}]
                        [{assign var="Windows" value=$Windows+$deviceKey->Aufrufe}]
                    [{elseif $deviceKey->Name == "iPhone"}]
                        [{assign var="iPhone" value=$iPhone+$deviceKey->Aufrufe}]
                    [{elseif $deviceKey->Name == "iPad"}]
                        [{assign var="iPad" value=$iPad+$deviceKey->Aufrufe}]
                    [{elseif $deviceKey->Name == "Kindle"}]
                        [{assign var="Kindle" value=$Kindle+$deviceKey->Aufrufe}]
                    [{elseif $deviceKey->Name == "Android"}]
                        [{assign var="Android" value=$Android+$deviceKey->Aufrufe}]
                    [{elseif $deviceKey->Name == "PlayBook"}]
                        [{assign var="PlayBook" value=$PlayBook+$deviceKey->Aufrufe}]
                    [{elseif $deviceKey->Name == "BlackBerry"}]
                        [{assign var="BlackBerry" value=$BlackBerry+$deviceKey->Aufrufe}]
                    [{elseif $deviceKey->Name == "Macintosh"}]
                        [{assign var="Macintosh" value=$Macintosh+$deviceKey->Aufrufe}]
                    [{elseif $deviceKey->Name == "Linux"}]
                        [{assign var="Linux" value=$Linux+$deviceKey->Aufrufe}]
                    [{elseif $deviceKey->Name == "Palm"}]
                        [{assign var="Palm" value=$Palm+$deviceKey->Aufrufe}]
                    [{/if}]
                    [{* Gerätetypen *}]

                    [{assign var="sum" value=$sum+$deviceKey->Aufrufe}]
                [{/foreach}]
            [{/foreach}]

            [{assign var="andere" value=$sum-$Macintosh-$Windows-$Android-$iPhone-$iPad-$Safari+$MacSafari+$WinSafari+$AndroidSafari+$iPhoneSafari+$iPadSafari-$Firefox+$MacFirefox+$WinFirefox+$AndroidFirefox+$iPhoneFirefox+$iPadFirefox-$Chrome+$MacChrome+$WinChrome+$AndroidChrome+$iPhoneChrome+$iPadChrome}]
            Aufrufe insgesamt: [{$sum}]
            <div class="row">
                <div class="container col-6" id="container"></div>
                <div class="container col-6" id="container2"></div>
            </div>
            <hr>
            <div class="row">
                <div class="container col-6" id="container3"></div>
                <div class="container col-6" id="container4"></div>
            </div>
            <hr>
            <div class="row">
                <table class="table table-striped">
                    <thead>
                    <td></td>
                    <th scope="col">Macintosh</th>
                    <th scope="col">Windows</th>
                    <th scope="col">Android</th>
                    <th scope="col">iPhone</th>
                    <th scope="col">iPad</th>
                    <th scope="col">Andere</th>
                    <th scope="col">Summe</th>
                    </thead>
                    <tbody>
                    <tr>
                        <th scope="row">Safari</th>
                        <td>[{$MacSafari}]</td>
                        <td>[{$WinSafari}]</td>
                        <td>[{$AndroidSafari}]</td>
                        <td>[{$iPhoneSafari}]</td>
                        <td>[{$iPadSafari}]</td>
                        <td>[{$Safari-$MacSafari-$WinSafari-$AndroidSafari-$iPhoneSafari-$iPadSafari}]</td>
                        <td>[{$Safari}]</td>
                    </tr>
                    <tr>
                        <th scope="row">Firefox</th>
                        <td>[{$MacFirefox}]</td>
                        <td>[{$WinFirefox}]</td>
                        <td>[{$AndroidFirefox}]</td>
                        <td>[{$iPhoneFirefox}]</td>
                        <td>[{$iPadFirefox}]</td>
                        <td>[{$Firefox-$MacFirefox-$WinFirefox-$AndroidFirefox-$iPhoneFirefox-$iPadFirefox}]</td>
                        <td>[{$Firefox}]</td>
                    </tr>
                    <tr>
                        <th scope="row">Chrome</th>
                        <td>[{$MacChrome}]</td>
                        <td>[{$WinChrome}]</td>
                        <td>[{$AndroidChrome}]</td>
                        <td>[{$iPhoneChrome}]</td>
                        <td>[{$iPadChrome}]</td>
                        <td>[{$Chrome-$MacChrome-$WinChrome-$AndroidChrome-$iPhoneChrome-$iPadChrome}]</td>
                        <td>[{$Chrome}]</td>
                    </tr>
                    <tr>
                        <th scope="row">Andere</th>
                        <td>[{$Macintosh-$MacSafari-$MacFirefox-$MacChrome}]</td>
                        <td>[{$Windows-$WindowsSafari-$WinFirefox-$WinChrome}]</td>
                        <td>[{$Android-$AndroidSafari-$AndroidFirefox-$AndroidChrome}]</td>
                        <td>[{$iPhone-$iPhoneSafari-$iPhoneFirefox-$iPhoneChrome}]</td>
                        <td>[{$iPad-$iPadSafari-$iPadFirefox-$iPadChrome}]</td>
                        <td>[{$andere}]</td>
                        <td>[{$sum-$Chrome-$Safari-$Firefox}]</td>
                    </tr>
                    <tr>
                        <th scope="row">Summe</th>
                        <td>[{$Macintosh}]</td>
                        <td>[{$Windows}]</td>
                        <td>[{$Android}]</td>
                        <td>[{$iPhone}]</td>
                        <td>[{$iPad}]</td>
                        <td>[{$sum-$Macintosh-$Windows-$Android-$iPhone-$iPad}]</td>
                        <td>[{$sum}]</td>
                    </tr>
                    </tbody>
                </table>

            </div>
        </div>
    </div>
</div>

<script>
    document.addEventListener('DOMContentLoaded', function () {
        var myChart = Highcharts.chart('container', {
            chart: {
                type: 'pie',
            },
            title: {
                text: 'Gerätetypen'
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
            },
            plotOptions: {
                series: {
                    events:  {

                    }
                },
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        style: {
                            color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                        }
                    }
                }
            },
            navigation: {
                buttonOptions: {
                    enabled: false
                }
            },
            series: [ {
                name: 'Anteil',
                colorByPoint: true,
                data: [ {
                    name : 'Windows Phone',
                    y : [{$WindowsPhone}]
                },{
                    name : 'Windows',
                    y : [{$Windows}]
                },{
                    name : 'iPhone',
                    y : [{$iPhone}]
                },{
                    name : 'iPad',
                    y : [{$iPad}]
                },{
                    name : 'Kindle',
                    y : [{$Kindle}]
                },{
                    name : 'Android',
                    y : [{$Android}]
                },{
                    name : 'PlayBook',
                    y : [{$PlayBook}]
                },{
                    name : 'BlackBerry',
                    y : [{$BlackBerry}]
                }, {
                    name: 'Macintosh',
                    y: [{$Macintosh}]
                },{
                    name : 'Linux',
                    y : [{$Linux}]
                },{
                    name : 'Palm',
                    y : [{$Palm}]

                } ]
            } ]
        });

        var myChart2 = Highcharts.chart('container2', {
            chart: {
                type: 'pie',
            },
            title: {
                text: 'Browser'
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
            },
            plotOptions: {
                series: {
                    events:  {

                    }
                },
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        style: {
                            color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                        }
                    }
                }
            },
            navigation: {
                buttonOptions: {
                    enabled: false
                }
            },
            series: [ {
                name: 'Anteil',
                colorByPoint: true,
                data: [ {
                    name : 'Chrome',
                    y : [{$Chrome}]
                },{
                    name : 'Firefox',
                    y : [{$Firefox}]
                },{
                    name : 'Safari',
                    y : [{$Safari}]
                },{
                    name : 'Internet Explorer',
                    y : [{$InternetExplorer}]
                },{
                    name : 'Opera',
                    y : [{$Opera}]
                },{
                    name : 'BlackBerry',
                    y : [{$BlackBerry}]
                },{
                    name : 'Mozilla',
                    y : [{$Mozilla}]
                } ]
            } ]
        });
        var myChart3 = Highcharts.chart('container3', {
            chart: {
                type: 'column'
            },
            title: {
                text: ''
            },
            xAxis: {
                categories: ['Windows Phone','Windows','iPhone','iPad','Kindle','Android','PlayBook','BlackBerry','Macintosh','Linux','Palm']

            },
            yAxis: {
                title: {
                    text: 'Anzahl'
                },
                //tickInterval : 20
            },
            series: [ {
                name: 'Gerätetypen',
                data: [
                    [{$WindowsPhone}],
                    [{$Windows}],
                    [{$iPhone}],
                    [{$iPad}],
                    [{$Kindle}],
                    [{$Android}],
                    [{$PlayBook}],
                    [{$BlackBerry}],
                    [{$Macintosh}],
                    [{$Linux}],
                    [{$Palm}]
                 ],
                color: 'red'
            } ]
        });

        var myChart4 = Highcharts.chart('container4', {
            chart: {
                type: 'column'
            },
            title: {
                text: ''
            },
            xAxis: {
                categories: ['Chrome','Firefox','Safari','Internet Explorer','Opera','BlackBerry','Mozilla'],
                labels: {
                    rotation: 315
                }
            },
            yAxis: {
                title: {
                    text: 'Anzahl'
                }
            },
            series: [ {
                name: 'Browser',
                data: [
                    [{$Chrome}],
                    [{$Firefox}],
                    [{$Safari}],
                    [{$InternetExplorer}],
                    [{$Opera}],
                    [{$BlackBerry}],
                    [{$Mozilla}]
                ],
                color: 'green'
            } ]
        });
    });

</script>
</body>

</html>




