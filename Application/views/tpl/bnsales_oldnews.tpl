[{capture append="oxidBlock_content"}]
    [{assign var="oConf" value=$oViewConf->getConfig()}]
<div class="card" style="margin-top: 180px;  height:auto !important; bottom:180px;" id="newsPanel">
    <div class="card-header">
        <h3 class="card-title"><strong>[{oxmultilang ident="ALLENEWS"}]</strong></h3>
        <div class="pull-right" style="margin-top: -27px;">
            [{if $oView->getClassName() eq "bnsales_oldnews" && !$blHideBreadcrumb}]
            [{include file="widget/breadcrumb.tpl"}]
            [{/if}]
        </div>
    </div>
    <div class="card-body">
        [{assign var="user" value=$oxcmp_user->oxuser__oxid->value}]
        <button id="buttonAllNews" type="button" class="btn btn-outline-primary bntooltip btn-sm" data-style="expand-right" onclick="openAllNews(this,'[{$user}]')" data-placement="top" title="[{oxmultilang ident="ALLENEWSZEIGEN"}]">
            <i class="fa fa-expand"></i>
        </button>
        [{assign var="bool" value=0}]
        [{assign var="counter" value=0}]
            [{assign var="list" value=$oView->initList()}]

            [{foreach from=$list item ='key'}]

                [{if $key->bnsales_news__oxactive->value == 1 || ($key->bnsales_news__oxactivefrom->value <= date('Y-m-d 00:00:00') && $key->bnsales_news__oxactiveto->value > date('Y-m-d 00:00:00') )}]

                [{assign var="oxid" value=$key->bnsales_news__oxid->value}]
                [{assign var="gelesen" value=$oView->gelesen($oxid,$user)}]
                    [{*if $gelesen == 1 && $bool == 0}]
                        <hr style="border-top:10px solid">
                            <h3>[{oxmultilang ident="ALTENEWS"}]</h3>
                        <hr style="border-top:10px solid">

                        [{assign var="bool" value=1}]
                        [{else}]
                        <hr/>
                    [{/if*}]
                    <hr/>
                    <div class="openNews" oxid="[{$oxid}]" id="alleNews[{$counter}]" onclick="openOldNews('[{$counter}]','[{$oxid}]','[{$user}]')" style="cursor:pointer">
                        <i id="newsFa[{$counter}]" class="fa fa-arrow-right"></i>
                        [{*if $gelesen == 0}]<b>[{/if}][{$key->bnsales_news__oxshortdesc->value}]<small style="margin-left:10px;[{if $gelesen == 0}]font-weight:bold !important;[{/if}]">[{$key->bnsales_news__oxdate|date_format:"%d.%m.%Y"}]</small>[{if $gelesen == 0}]</b>[{/if*}]
                        <span id="newsSpan[{$counter}]"  style="[{*if $gelesen == 0*}]font-weight:bold !important[{*/if*}]">[{if $gelesen == 0}]<i id="bluePoint[{$counter}]" style="color:#467bff" class="fas fa-circle"></i>[{/if}] [{$key->bnsales_news__oxshortdesc->value}]<small id="newsSpanChild[{$counter}]" style="margin-left:10px;[{if $gelesen == 0}]font-weight:bold;[{/if}]">[{$key->bnsales_news__oxdate|date_format:"%d.%m.%Y"}]</small></span>
                    </div>
                    <div id="newsBox[{$counter}]" class="newsbox" style="display:none">
                        [{$key->bnsales_news__oxlongdesc->value|html_entity_decode}]
                    </div>
                [{/if}]
                [{assign var="counter" value=$counter+1}]
            [{/foreach}]
        [{*foreach from=$news item='newsItem'}]
            [{if $newsItem.OXUSERID == $oxcmp_user->oxuser__oxid->value}]
                [{if $newsItem.OXACTIVE == 1 || ($newsItem.OXACTIVEFROM < date('Y-m-d') && date('Y-m-d') < $newsItem.OXACTIVETO)}]
                [{if $news[$counter].gelesen == 1 && $bool == 0}]
                <hr style="border-top:10px solid">
                    <h3>[{oxmultilang ident="ALTENEWS"}]</h3>
                <hr style="border-top:10px solid">

                [{assign var="bool" value=1}]
                [{else}]
                <hr/>
                [{/if}]
                <b>[{$newsItem.$short}]<small style="margin-left:10px">[{$newsItem.OXDATE|date_format:"%d.%m.%Y"}]</small></b>
                <div id="newsBox" class="newsbox">
                    [{$newsItem.$long}]
                </div>
             [{/if}]
            [{/if}]
        [{assign var="counter" value = $counter+1}]

        [{/foreach*}]
    </div>
</div>
[{/capture}]

[{include file="layout/page.tpl"}]

