<?php
/**
 * This file is part of OXID eSales PayPal module.
 *
 * OXID eSales PayPal module is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OXID eSales PayPal module is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OXID eSales PayPal module.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2018
 */

// -------------------------------
// RESOURCE IDENTIFIER = STRING
// -------------------------------
$aLang = array(
    'charset'                                            => 'UTF-8',
    'SHOP_MODULE_GROUP_bnsales_main'                 => 'Allgemein',
    'SHOP_MODULE_GROUP_bnsales_feiertage'                 => 'Bundesländer Feiertage',
    'SHOP_MODULE_GROUP_bnsales_editnews'                 => 'Neue News',
    'SHOP_MODULE_bnsales_oxgroup'                 => 'Benutzergruppe',
    'SHOP_MODULE_bnsales_oxgroup_Gruppe_1'                 => 'Gruppe 1',
    'SHOP_MODULE_bnsales_oxgroup_Gruppe_2'                 => 'Gruppe 2',
    'SHOP_MODULE_bnsales_oxgroup_Gruppe_3'                 => 'Gruppe 3',
    'SHOP_MODULE_bnsales_oxgroup_alle'                 => 'Alle',
    'SHOP_MODULE_bnsales_oxshortdesc'                 => 'Titel',
    'SHOP_MODULE_bnsales_oxlongdesc'                 => 'Inhalt',
    'SHOP_MODULE_bnsales_oxactive'                 => 'Aktiv',
    'SHOP_MODULE_bnsales_oxdate'                 => 'Datum',
    'SHOP_MODULE_bnsales_oxactivefrom'                 => 'Aktiv von',
    'SHOP_MODULE_bnsales_oxactiveto'                 => 'Aktiv bis',
    'SHOP_MODULE_email_news'                            => 'Email-Adresse Kundenkommentare',
    'SHOP_MODULE_Feiertage_NRW'                            => 'Feiertage NRW',
    'SHOP_MODULE_Feiertage_BW'                            => 'Feiertage BW',
    'SHOP_MODULE_zeitspanne_feiertag'                            => 'Zeithorizont',
    'SHOP_MODULE_urlFeiertage'                            => '<a target="_blank" href="https://sales.bodynova.com/modules/bodynova/bnsales_news/Core/getFeiertageAPI.php">Feiertage eintragen</a><span style="color:red;margin-left:20px" >Nur einmal pro Jahr ausführen!! Mit Admin absprechen</span>'

);
