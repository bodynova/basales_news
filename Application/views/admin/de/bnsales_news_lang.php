<?php
/**
 * Created by PhpStorm.
 * User: andre
 * Date: 2019-03-07
 * Time: 13:58
 */

$sLangName = "Deutsch";
$iLangNr = 0;
$aLang = array(
    'charset'                                  => 'UTF-8 ',
    'BNSALES_NEWS_MENU_NEWS'                   => 'Neuigkeiten',
    'BNSALES_NEWS_SUBMENU_NEWS'                   => 'Neuigkeiten',
    'BNSALES_NEWS_MENU_ANALYZE'                   => 'Benutzeranalyse',
    'BNSALES_NEWS_SUBMENU_ANALYZE'                   => 'Benutzeranalyse'
);