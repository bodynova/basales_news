<?php
namespace Bodynova\bnSales_News\Application\Controller;

use OxidEsales\Eshop\Core\Registry;
use OxidEsales\EshopProfessional\Core\DatabaseProvider;


class bnsales_oldnews extends \OxidEsales\Eshop\Application\Controller\FrontendController{
    protected $_sThisTemplate = 'bnsales_oldnews.tpl';



    public function render()
    {
        parent::render();

        $oUser = $this->getUser();
        //$this->_sUserOxid = $oUser;
        $regobject = Registry::getConfig();

        if ($oUser == null) {
            \OxidEsales\Eshop\Core\Registry::getUtils()->redirect($regobject->getShopHomeURL() . 'cl=account&sourcecl=start');
        }
        return $this->_sThisTemplate;
    }

    /**
     * Returns Bread Crumb - you are here page1/page2/page3...
     *
     * @return array
     */
    public function getBreadCrumb()
    {
        $aPaths = array();
        $aPath = array();
        $oLang = \OxidEsales\Eshop\Core\Registry::getLang();
        $iBaseLanguage = $oLang->getBaseLanguage();

        $aPath['title'] = $oLang->translateString('ALLENEWS', $iBaseLanguage, false);

        $aPath['link'] = $this->getLink();

        $aPaths[] = $aPath;

        return $aPaths;
    }

    public function show(){

        $query = 'SELECT * FROM bnsales_news AS a
                  LEFT JOIN oxuser2news AS b ON a.OXID = b.OXNEWSID
				  WHERE b.OXUSERID = ?
                  ORDER BY OXDATE DESC';
        try{
            $oDb = DatabaseProvider::getDb(DatabaseProvider::FETCH_MODE_ASSOC);
            $result = $oDb->getAll($query,array($this->getUser()->oxuser__oxid->value));
            $result['OXSHORTDESC'] = Registry::getLang()->translateString($result['OXSHORTDESC']);
            $result['OXLONGDESC'] = Registry::getLang()->translateString($result['OXLONGDESC']);

            $this->_aViewData['news'] = $result;

            /*echo '<pre>';
            print_r($result);
            die();*/
            //return $result;
        } catch(\Exception $e){
            echo $e->getMessage() , "\n";
        }
    }

    /**
     * Ein Objekt der Klasse List wird erstellt, dann wird getList() ausgeführt, um die OXIDs der News zu erhalten.
     * Für jeden Eintrag des Arrays wird die jeweilige Benutzerbezogene News mit der Funktion initNews() erstellt.
     */
    public function initList(){
        $ob = oxNew(\Bodynova\bnSales_News\Application\Model\bnsales_list::class);

        $a = array();

        $arrTest = $ob->getList();
        foreach($arrTest as $key){
            array_push($a,$this->initNews($key['OXID']));
        }
        return $a;
    }

   public function initNews($oxid){
        $ob = oxNew(\Bodynova\bnSales_News\Application\Model\bnsales_news::class);
        $ob->getOldNews($oxid);
        return $ob;
   }

   /**
    * Funktion für gelesen
    */

   public function gelesen($oxid,$user){
       $arrOxid = array($oxid,$user);
       $query = 'SELECT gelesen FROM bnsales_news AS a
                  LEFT JOIN oxuser2news AS b ON a.OXID = b.OXNEWSID WHERE a.OXID = ? AND b.OXUSERID = ?';
       try{
           $oDb = DatabaseProvider::getDb(DatabaseProvider::FETCH_MODE_ASSOC);
           $result = $oDb->getOne($query,$arrOxid);
           return $result;
       } catch(\Exception $e){
           echo $e->getMessage() , "\n";
       }
   }

   public function updateGelesen(){

       if(isset($_POST['oxid']) && isset($_POST['user'])){
           $oxid = $_POST['oxid'];
           $user = $_POST['user'];
           $query = 'UPDATE oxuser2news SET gelesen = 1 WHERE OXNEWSID = ? AND OXUSERID = ?';
           $arrQuery = array($oxid,$user);
           try{
               $oDb = DatabaseProvider::getDb(DatabaseProvider::FETCH_MODE_ASSOC);
               $oDb->execute($query,$arrQuery);
               return;
           } catch(\Exception $e){
               echo 'ERROR: ' . $e->getMessage() . "\n";
           }
       }
   }

   public function updateGelesenAll(){

        if(isset($_POST['oxid']) && isset($_POST['user'])){
            $oxid = $_POST['oxid'];
            $user = $_POST['user'];

            $query = 'UPDATE oxuser2news SET gelesen = 1 WHERE OXNEWSID IN '.$oxid.' AND OXUSERID = "'.$user . '"';

            try{
                $oDb = DatabaseProvider::getDb(DatabaseProvider::FETCH_MODE_ASSOC);
                $oDb->execute($query);

            } catch(\Exception $e){
                echo 'ERROR: ' . $e->getMessage() . "\n";
                die();
            }
        }
   }

}