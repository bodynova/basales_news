<?php


namespace Bodynova\bnSales_News\Application\Controller;


use stdClass;

class bnsales_mainuser extends bnsales_mainuser_parent
{
    public function render()
    {
        parent::render();

        // malladmin stuff
        $oAuthUser = oxNew(\OxidEsales\Eshop\Application\Model\User::class);
        $oAuthUser->loadAdminUser();
        $blisMallAdmin = $oAuthUser->oxuser__oxrights->value == "malladmin";

        // User rights
        $aUserRights = [];
        $oLang = \OxidEsales\Eshop\Core\Registry::getLang();
        $iTplLang = $oLang->getTplLanguage();

        $iPos = count($aUserRights);
        $aUserRights[$iPos] = new stdClass();
        $aUserRights[$iPos]->name = $oLang->translateString("user", $iTplLang);
        $aUserRights[$iPos]->id = "user";

        if ($blisMallAdmin) {
            $iPos = count($aUserRights);
            $aUserRights[$iPos] = new stdClass();
            $aUserRights[$iPos]->id = "malladmin";
            $aUserRights[$iPos]->name = $oLang->translateString("Admin", $iTplLang);
        }

        $aUserRights = $this->calculateAdditionalRights($aUserRights);

        $soxId = $this->_aViewData["oxid"] = $this->getEditObjectId();
        if (isset($soxId) && $soxId != "-1") {
            // load object
            $oUser = oxNew(\OxidEsales\Eshop\Application\Model\User::class);
            $oUser->load($soxId);
            $this->_aViewData["edit"] = $oUser;

            if (!($oUser->oxuser__oxrights->value == "malladmin" && !$blisMallAdmin)) {
                // generate selected right
                reset($aUserRights);
                foreach ($aUserRights as $val) {
                    if ($val->id == $oUser->oxuser__oxrights->value) {
                        $val->selected = 1;
                        break;
                    }
                }
            }
        }

        // passing country list
        $oCountryList = oxNew(\OxidEsales\Eshop\Application\Model\CountryList::class);
        $oCountryList->loadActiveCountries($oLang->getObjectTplLanguage());

        $this->_aViewData["countrylist"] = $oCountryList;

        $this->_aViewData["rights"] = $aUserRights;

        if ($this->_sSaveError) {
            $this->_aViewData["sSaveError"] = $this->_sSaveError;
        }

        if (!$this->_allowAdminEdit($soxId)) {
            $this->_aViewData['readonly'] = true;
        }
        if (\OxidEsales\Eshop\Core\Registry::getConfig()->getRequestParameter("aoc")) {
            $oUserMainAjax = oxNew(\OxidEsales\Eshop\Application\Controller\Admin\UserMainAjax::class);
            $this->_aViewData['oxajax'] = $oUserMainAjax->getColumns();
            return "popups/user_main.tpl";
        }
        return 'bnsales_user.tpl';
    }

}