<?php
namespace Bodynova\bnSales_News\Application\Controller;

use OxidEsales\Eshop\Core\Registry;
use OxidEsales\Eshop\Core\Request;
use OxidEsales\Eshop\Core\DatabaseProvider;
use OxidEsales\Eshop\Core\UtilsObject;

class bnsales_mynews extends \OxidEsales\Eshop\Application\Controller\FrontendController
{


    // teplate for the view
    protected $_sThisTemplate = 'bnsales_mynews.tpl';

    protected $_sUserOxid = null;

    public function render()
    {
        parent::render();

        $oUser = $this->getUser();
        //$this->_sUserOxid = $oUser;
        $regobject = Registry::getConfig();

        if ($oUser == null) {
            \OxidEsales\Eshop\Core\Registry::getUtils()->redirect($regobject->getShopHomeURL() . 'cl=account&sourcecl=start');
        }
        return $this->_sThisTemplate;
    }


    public function showLists()
    {
        $this->render();
        $this->_aViewData['myuser'] = $this->getUnreadMessages($_GET['lang']);
        //$this->_aViewData['myuser'] = $this->getUser();

    }

    public function showAnzahl(){

        $oUser = $this->getUser()->oxuser__oxid->value;
        $arrUser = array($oUser);
        $lang = $_GET['lang'];
        if($lang == 0){
            $lang = '';
        } else {
            $lang = '_' . $lang;
        }
        $query = 'SELECT COUNT(*) AS Anzahl FROM bnsales_news AS a
                  LEFT JOIN oxuser2news AS b ON a.OXID = b.OXNEWSID
                  WHERE b.gelesen = 0 AND b.OXUSERID = ? AND (a.OXACTIVE' . $lang . ' = 1 OR (OXACTIVEFROM <CURRENT_TIMESTAMP AND OXACTIVETO >CURRENT_TIMESTAMP))';
        try{
            $oDb = DatabaseProvider::getDb(DatabaseProvider::FETCH_MODE_NUM);
            $result = $oDb->getAll($query,$arrUser);
            $this->_aViewData['ungelesen'] = $result[0];
            print_r($result[0][0]);
            die();
        } catch(\Exception $e){
            echo $e->getMessage() , "\n";
        }
    }

    public function getUnreadMessages($lang = null){

        $oUser = $this->getUser()->oxuser__oxid->value;
        $arrUser = array($oUser);
        if($lang == 0){
            $lang = '';
        } else {
            $lang = '_' . $lang;
        }
        $query = 'SELECT * FROM bnsales_news AS a
                  LEFT JOIN oxuser2news AS b ON a.OXID = b.OXNEWSID
                  WHERE b.gelesen = 0 AND b.OXUSERID = ? AND (a.OXACTIVE' . $lang . ' = 1 OR (OXACTIVEFROM <CURRENT_TIMESTAMP AND OXACTIVETO >CURRENT_TIMESTAMP)) ORDER BY OXDATE DESC';
        try{
            $oDb = DatabaseProvider::getDb(DatabaseProvider::FETCH_MODE_ASSOC);
            $result = $oDb->getAll($query,$arrUser);
            $this->_aViewData['myuser'] = $result;
            return $result;
        } catch(\Exception $e){
            echo $e->getMessage() , "\n";
        }
    }

    public function getNext(){
        $oxid = $_GET['oxid'];
        $oUser = $this->getUser()->oxuser__oxid->value;

        $queryUpdate = 'UPDATE oxuser2news SET gelesen = 1 WHERE OXID =? AND OXUSERID =?';
        try{
            $oDb = DatabaseProvider::getDb(DatabaseProvider::FETCH_MODE_ASSOC);
            $oDb->execute($queryUpdate,array($oxid,$oUser));
        } catch(\Exception $e){
            echo $e->getMessage() , "\n";
        }
        //$this->_aViewData['myuser'] = $this->getUnreadMessages();
        $this->render();
    }

    // FROM TO Active !

    public function getPrev(){

        $querySelect = 'SELECT * FROM bnsales_news AS a
                        LEFT JOIN oxuser2news AS b ON a.OXID = b.OXNEWSID
                         WHERE b.gelesen = 1 AND (a.OXACTIVE = 1 OR (OXACTIVEFROM <CURRENT_TIMESTAMP AND OXACTIVETO >CURRENT_TIMESTAMP)) ORDER BY OXDATE DESC';
        try{
            $oDb = DatabaseProvider::getDb(DatabaseProvider::FETCH_MODE_ASSOC);
            $result = $oDb->getAll($querySelect);
            echo '<pre>';
            print_r($result);
            $oxid = $result[0]['OXID'];
            $queryUpdate = 'UPDATE oxuser2news SET gelesen = 0 WHERE OXID =?';
            $oDb->execute($queryUpdate,array($oxid));
        } catch(\Exception $e){
            echo $e->getMessage() , "\n";
        }
        $this->render();
    }

    public function commentEmail(){
        $comment = $_GET['comment']; //Registry::get(Request::class)->getRequestEscapedParameter('comment');
        if($comment == ""){
            die(Registry::getLang()->translateString('AbortComment'));
        }
        $oxid =  $_GET['oxid'];//Registry::get(Request::class)->getRequestEscapedParameter('oxid');
        $email =  $_GET['email'];//Registry::get(Request::class)->getRequestEscapedParameter('userEmail');
        $vorname = $_GET['vorname'];  //Registry::get(Request::class)->getRequestEscapedParameter('vorname');
        $nachname = $_GET['nachname'];  //Registry::get(Request::class)->getRequestEscapedParameter('nachname');
        $firma = $_GET['firma'];  //Registry::get(Request::class)->getRequestEscapedParameter('firma');

        $sendTo = Registry::getConfig()->getConfigParam('email_news');
        $oxEmail = oxNew(\OxidEsales\Eshop\Core\Email::class);
        $oxEmail->setSubject("Userkommentar zu News: " . $oxid );
        $oxEmail->setBody($comment);
        $oxEmail->setReplyTo($email, $vorname . ' ' . $nachname . ' ' . $firma);
        $oxEmail->setRecipient($sendTo, 'Sales Bodynova');
        $oxEmail->setFrom($email);
        $oxEmail->send();

        $return = Registry::getLang()->translateString('ConfirmComment');
        echo $return;
        die();
    }


    public function createNews(){

        // Verarbeite alle Daten, initialisiere alle relevanten Variablen
        $active =Registry::getConfig()->getConfigParam('bnsales_oxactive');
        $activeFrom =Registry::getConfig()->getConfigParam('bnsales_oxactivefrom');
        $activeTo =Registry::getConfig()->getConfigParam('bnsales_oxactiveto');
        $date =Registry::getConfig()->getConfigParam('bnsales_oxdate');
        $title =Registry::getConfig()->getConfigParam('bnsales_oxshortdesc');
        $inhalt =Registry::getConfig()->getConfigParam('bnsales_oxlongdesc');


        /*
        if(isset($_POST['active'])){
            $active = $_POST['active'];
        } else $active = '0';
        if(isset($_POST['oxdate'])){
            $date = $_POST['oxdate'];
        } else $date = date('Y-m-d');
        if(isset($_POST['activeFrom'])){
            $activeFrom = $_POST['activeFrom'];
        } else $activeFrom = '0000-00-00';
        if(isset($_POST['activeTo'])){
            $activeTo = $_POST['activeTo'];
        } else $activeTo = '0000-00-00';
        if(isset($_POST['title'])){
            $title = $_POST['title'];
        } else die();
        if(isset($_POST['inhalt'])){
            $inhalt = $_POST['inhalt'];
        } else die(); */
        $oxid = 'Feiertag' . substr(UtilsObject::getInstance()->generateUId(),8);
        $shopid = '1';

        // Baue Insert-SQL auf
        $arrNews = array($oxid,$shopid,$active,$activeFrom,$activeTo,$date,$title,$inhalt);

        $query = "INSERT INTO `bnsales_news`(`OXID`, `OXSHOPID`, `OXACTIVE`, `OXACTIVEFROM`, `OXACTIVETO`, `OXDATE`, `OXSHORTDESC`, `OXLONGDESC`) VALUES (?,?,?,?,?,?,?,?)";

        try{
            $oDb = DatabaseProvider::getDb(DatabaseProvider::FETCH_MODE_ASSOC);
            $oDb->execute($query,$arrNews);

            // TODO: Foreach Schleife, die jedem User diese eine Neuigkeit hinzufügt. ODER??? gibt es News, die nur für bestimmte Benutzergruppen sein sollen
            $arrUserSQL = 'SELECT OXID FROM oxuser WHERE OXACTIVE =1';
            $arrUser = $oDb->getAll($arrUserSQL);
            foreach($arrUser as $item) {
                $updateQueryNewsUser = 'INSERT INTO oxuser2news(`OXID`,`OXSHOPID`,`OXUSERID`,`OXNEWSID`,`gelesen`) VALUES (?,?,?,?,?)';
                $oxidKreuzTabelle = 'Feiertag' . substr(\OxidEsales\Eshop\Core\UtilsObject::getInstance()->generateUID(), 8);
                $OXSHOPID = 1;
                $oxuserid = $item['OXID'];
                $newsid = $oxid;
                $arrayKreuztabelle = array(
                    $oxidKreuzTabelle,
                    $OXSHOPID,
                    $oxuserid,
                    $newsid,
                    0
                );
                $oDb->execute($updateQueryNewsUser, $arrayKreuztabelle);
            }
        } catch(\Exception $e){
            echo 'Error' . $e->getMessage() . "\n";
        }
    }


}