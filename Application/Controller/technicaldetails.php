<?php
/**
 * Copyright © OXID eSales AG. All rights reserved.
 * See LICENSE file for license details.
 */

namespace Bodynova\bnSales_News\Application\Controller;
use OxidEsales\Eshop\Application\Controller\NewsController;
use OxidEsales\Eshop\Core\DatabaseProvider;
use OxidEsales\Eshop\Core\Registry;
use OxidEsales\Eshop\Core\UtilsObject;
use oxRegistry;
use oxDb;
use oxField;
use stdClass;
use OxidEsales\Eshop\Application\Model\Article;

/**
 * Admin article main manager.
 * Collects and updates (on user submit) article base parameters data ( such as
 * title, article No., short Description and etc.).
 * Admin Menu: Manage Products -> Articles -> Main.
 */

class technicaldetails extends \OxidEsales\Eshop\Application\Controller\Admin\AdminDetailsController
{
    protected $_sThisTemplate = 'article_technical_details.tpl';

    public function render() {
        parent::render();
        $this->_aViewData['edit'] = $article = oxNew(\OxidEsales\Eshop\Application\Model\Article::class);
        $oxId = $this->getEditObjectId();
        if (isset($oxId) && $oxId != "-1") {
            $article->loadInLang($this->_iEditLang, $oxId);
        }
        return $this->_sThisTemplate;
    }

    public function save() {
        $soxId = $this->getEditObjectId();
        $aParams = \OxidEsales\Eshop\Core\Registry::getConfig()->getRequestParameter("editval");

        $oArticle = oxNew(\OxidEsales\Eshop\Application\Model\Article::class);
        $oArticle->loadInLang($this->_iEditLang, $soxId);
        $oArticle->setLanguage(0);
        $oArticle->assign($aParams);
        $oArticle->setLanguage($this->_iEditLang);
        $oArticle = \OxidEsales\Eshop\Core\Registry::getUtilsFile()->processFiles($oArticle);
        $oArticle->save();

        parent::save();
    }

    public function getUsergroups(){
        $query = 'SELECT OXID,OXTITLE FROM oxgroups';
        $oDb = DatabaseProvider::getDb(DatabaseProvider::FETCH_MODE_ASSOC);
        try{
            $result = $oDb->getAll($query);
            return $result;
        } catch(\Exception $e){
            echo 'Error : ' . $e->getMessage() . "\n";
        }
    }

    public function getNews(){
        $query = 'SELECT * FROM bnsales_news ORDER BY OXDATE DESC';
        $oDb = DatabaseProvider::getDb(DatabaseProvider::FETCH_MODE_ASSOC);
        try{
            $result = $oDb->getAll($query);
            return $result;
        } catch(\Exception $e){
            echo 'Error : ' . $e->getMessage() . "\n";
        }
    }

    /**
     * schreibt erstellte News aus dem Backend in die Datenbank.
     */
    public function NewsBackend(){


        $oDb = DatabaseProvider::getDb(DatabaseProvider::FETCH_MODE_ASSOC);
        echo '<pre>';
        $gruppen = $_POST['gruppenSelect'];
        print_r($gruppen[0]);
        if($gruppen[0] == 'Alle'){
            $SQLgruppe = 'SELECT DISTINCT OXOBJECTID FROM oxobject2group as a INNER JOIN oxuser as b ON a.OXOBJECTID=b.OXID';
        } else {
            $gruppen = implode("','",$gruppen);
            $gruppen = "'" . $gruppen . "'";
            $SQLgruppe = 'SELECT DISTINCT OXOBJECTID FROM oxobject2group as a INNER JOIN oxuser as b ON a.OXOBJECTID=b.OXID WHERE a.OXOBJECTID=b.OXID AND OXGROUPSID IN ('.$gruppen.')';
        }
        echo '<br>' . $SQLgruppe;
        try{
            $userGruppen = $oDb->getAll($SQLgruppe);
        }catch(\Exception $e){
            echo 'Error' . $e->getMessage() . "\n";
        }
        $oxid = UtilsObject::getInstance()->generateUId();
        $active = $_POST['active'];
        $active_1 = $_POST['active_1'];
        $active_2 = $_POST['active_2'];
        $date = $_POST['date'];
        $dateFrom = $_POST['dateFrom'];
        $dateTo = $_POST['dateTo'];
        $title = $_POST['title'];
        $content = $_POST['content'];
        $title_1 = $_POST['title_1'];
        $content_1 = $_POST['content_1'];
        $title_2 = $_POST['title_2'];
        $content_2 = $_POST['content_2'];

        if($active == 'on' || $active_1 == 'on' || $active_2 == 'on'){
            $dateFrom = '0000-00-00';
            $dateTo = '0000-00-00';
        }

        if($active == 'on'){$active = 1;}else $active =0;
        if($active_1 == 'on'){$active_1 = 1;}else $active_1 =0;
        if($active_2 == 'on'){$active_2 = 1;}else $active_2 =0;

        $arrNews = array($oxid,1,$active,$active_1,$active_2,$dateFrom,$dateTo,$date,$title,$content,$title_1,$content_1,$title_2,$content_2);

        $query = "INSERT INTO `bnsales_news`(`OXID`, `OXSHOPID`, `OXACTIVE`,`OXACTIVE_1`,`OXACTIVE_2`, `OXACTIVEFROM`, `OXACTIVETO`, `OXDATE`, `OXSHORTDESC`, `OXLONGDESC` , `OXSHORTDESC_1`, `OXLONGDESC_1` , `OXSHORTDESC_2`, `OXLONGDESC_2`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

        try{
            $oDb->execute($query,$arrNews);
            foreach($userGruppen as $key){
                $updateQueryNewsUser = 'INSERT INTO oxuser2news(`OXID`,`OXSHOPID`,`OXUSERID`,`OXNEWSID`,`gelesen`) VALUES (?,?,?,?,?)';
                $oxidKreuzTabelle = \OxidEsales\Eshop\Core\UtilsObject::getInstance()->generateUID();
                $OXSHOPID = 1;
                $oxuserid = $key['OXOBJECTID'];
                $newsid = $oxid;
                $arrayKreuztabelle = array(
                    $oxidKreuzTabelle,
                    $OXSHOPID,
                    $oxuserid,
                    $newsid,
                    0
                );
                $oDb->execute($updateQueryNewsUser, $arrayKreuztabelle);
            }
        }catch(\Exception $e){
            echo 'Error: ' . $e->getMessage() . "\n";
            die();
        }
        $sLink = Registry::getConfig()->getCurrentShopUrl(true) . 'index.php?cl=TechnicalDetails&stoken=' . Registry::getConfig()->getRequestParameter("stoken") . '&force_admin_sid=' . Registry::getConfig()->getRequestParameter("force_admin_sid") ;
        Registry::getUtils()->redirect($sLink);

    }

    /**
     * Funktion zur Speicherung von Änderungen im Backend
     */
    public function saveNews(){
        if(Registry::getConfig()->getRequestParameter("inputActive") == ""){
            $active = 0;
        } else $active = 1;
        $lang = Registry::getConfig()->getRequestParameter("lang");
        $oDb = DatabaseProvider::getDb(DatabaseProvider::FETCH_MODE_ASSOC);
        $sUpdate = 'Update bnsales_news SET OXACTIVE' . $lang . ' = ?, OXDATE = ?,OXSHORTDESC' . $lang . ' = ?,OXLONGDESC' . $lang . ' = ? WHERE OXID = ?';
        $arrUpdate = array(
            $active,
            Registry::getConfig()->getRequestParameter("inputDate"),
            Registry::getConfig()->getRequestParameter("inputShort".$lang),
            Registry::getConfig()->getRequestParameter("inputLong".$lang),
            Registry::getConfig()->getRequestParameter("oxid")
        );
        try{
            $oDb->execute($sUpdate,$arrUpdate);
        }catch(\Exception $e){
            echo 'Error: ' . $e->getMessage() . "\n";
        }

        $this->_aViewData['oxid'] = Registry::getConfig()->getRequestParameter("oxid");
        $this->_aViewData['lang'] = Registry::getConfig()->getRequestParameter("lang");

    }


}