<?php
namespace Bodynova\bnSales_News\Application\Controller;
use OxidEsales\Eshop\Core\Registry;
use OxidEsales\Eshop\Core\Request;
use OxidEsales\Eshop\Core\DatabaseProvider;
use OxidEsales\Eshop\Core\UtilsObject;


class analyze extends analyze_parent
{
    protected $_sThisTemplate = 'analyze.tpl';

    public function render(){
        parent::render();
        return $this->_sThisTemplate;
    }

    public function getData(){

        $oDb = DatabaseProvider::getDb(DatabaseProvider::FETCH_MODE_NUM);
        $query = 'SELECT DeviceType FROM oxuser WHERE DeviceType <> ""';
        $result = $oDb->getAll($query);
        return $result;
    }

}