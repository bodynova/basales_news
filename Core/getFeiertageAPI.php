<?php

use OxidEsales\Eshop\Core\Registry;
require_once 'feiertage.php';
include '../../../../bootstrap.php';

$feiertagsObjekt = new feiertage();

// Feiertage aus API mit Jahr und Land
$jahr = date('Y');
$land1 =Registry::getConfig()->getConfigParam('Feiertage_NRW');
$land2 =Registry::getConfig()->getConfigParam('Feiertage_BW');
$arrLaender = array($land1,$land2);
$arrFeiertage = $feiertagsObjekt->getApiFeiertage($jahr,$arrLaender);

// Feiertage aus API mit Jahr und Land

// Datumsberechnung Heute als UNIX zur Berechnung
$today = date_create(date('Y-m-d'));
$time2event = Registry::getConfig()->getConfigParam('zeitspanne_feiertag');
$heutePlusX = $feiertagsObjekt->getZeitSpanne($time2event,$today);
// Datumsberechnung Heute als UNIX zur Berechnung


// Für jeden Eintrag/Feiertag prüfen wir, ob der Feiertag in der Zukunft liegt, wenn ja führe SQL-Befehle aus, um news zu generieren und die Kreuztabelle zu füllen.

foreach($arrFeiertage as $key=>$feiertag){
    $unixFeiertag = strtotime($feiertag['datum']);

    if($unixFeiertag-$today->getTimestamp()>(($time2event * 86400)*-1)) {
        if (($unixFeiertag - $heutePlusX) > $time2event * 86400) { //In diesem If-Körper legen wir die Werte für die Datenbankabfrage fest!
            $arrFromTo = $feiertagsObjekt->getDateFromTo($feiertag);
            // Erstelle Array für OXSHORTDESC und OXLONGDESC
            $arrOxDescription = $feiertagsObjekt->getOxDescription($feiertag);
            //SQL-Execute
            $feiertagsObjekt->executeSQL($arrOxDescription,$arrFromTo,$feiertag);
        } else {
            echo 'Event liegt in der Vergangenheit';
        }
    } else {
        echo 'Event liegt in der Vergangenheit';
    }
}

// Alle OXNEWS-Feiertage, deren OXACTIVETO > TODAY sind, werden aus der NewsTabelle Gelöscht.
$feiertagsObjekt->deleteSQL();



