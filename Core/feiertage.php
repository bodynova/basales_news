<?php
use OxidEsales\Eshop\Core\DatabaseProvider;
use OxidEsales\Eshop\Core\Registry;
include 'Feiertage_Connector.php';

class feiertage
{
    /**
     * Hier werden Feiertage gesammelt. Zuerst die aus NRW, falls in BW Feiertage auf das gleiche Datum fallen,
     * werden sie aussortiert.
     * @param $jahr 2019,2020,...
     * @param $laender .NRW,BW
     * @return array $arrFeiertage mit allen Feiertagen
     */

    public function getApiFeiertage ($jahr,$laender){
        $connector = Feiertage_Connector::getInstance();
        $arrFeiertage = array();

        if($laender[0] == 1){
            //$arrFeiertage[0] = $connector->getFeiertageVonLand($jahr, 'NW');
            foreach($connector->getFeiertageVonLand($jahr, 'NW') as $item){
                array_push($arrFeiertage,$item);
            }
            //array_push($arrFeiertage, $connector->getFeiertageVonLand($jahr, 'NW'));
        }
        if($laender[1] == 1){
            //$arrFeiertage[1] = $connector->getFeiertageVonLand($jahr, 'BW');
            //array_push($arrFeiertage, $connector->getFeiertageVonLand($jahr, 'BW'));
            foreach($connector->getFeiertageVonLand($jahr, 'BW') as $item){
                if(!(in_array($item,$arrFeiertage))){
                    array_push($arrFeiertage,$item);
                }
            }
        }
        return $arrFeiertage;
    }

    /**
     * @return 'Datum' von Heute mit Berücksichtung der Karenzzeit
     */
    public function getZeitSpanne($time2event,$today){
        $heutePlusX = date_add($today, date_interval_create_from_date_string($time2event." days"));
        $heutePlusX=$heutePlusX->getTimestamp();
        return $heutePlusX;
    }


    public function KeineAhnungWasIchMache(){
        return 'Dummes Gewäsch!';
    }

    /**
     * @param $feiertag 'aktueller Feiertag in der Foreach schleife
     * @return array Mit Datumsspanne die zum Vergleich verwendet werden kann
     * @throws Exception
     */
    public function getDateFromTo($feiertag){

        $arrFromTo = array();
        $time2event = Registry::getConfig()->getConfigParam('zeitspanne_feiertag');
        $dateFrom = new DateTime(date('Y-m-d',strtotime($feiertag['datum'])));
        $dateTo = new DateTime(date('Y-m-d',strtotime($feiertag['datum'])));

        $dateFrom = $dateFrom->sub(DateInterval::createFromDateString($time2event.' days'));
        $dateFrom = $dateFrom->format('Y-m-d 00:00:00');

        $dateTo = $dateTo->add(DateInterval::createFromDateString('1 days'));
        $dateTo = $dateTo->format('Y-m-d 00:00:00');

        $arrFromTo[0] = $dateFrom;
        $arrFromTo[1] = $dateTo;

        return $arrFromTo;
    }

    /**
     * Hier wird eine OXID vergeben plus Titel und Beschreibung
     * @param $feiertag
     * @return mixed
     */
    public function getOxDescription($feiertag){
        $arrOxDescritpion = array();
        $arrOxDescription[0] = 'Feiertag'.substr(\OxidEsales\Eshop\Core\UtilsObject::getInstance()->generateUId(),8);
        $arrOxDescription[1] = 'Feiertag!';
        $arrOxDescription[2] = 'Aufgrund eines Feiertages sind unsere Büros am ' . date('d.m.Y',strtotime($feiertag['datum'])) . ' geschlossen. Wir sind per Telefon und Email nicht erreichbar, am nächstfolgenden Arbeitstag sind wir dann wieder wie gewohnt für Sie da.';
        $arrOxDescription[3] = 'Holiday!';
        $arrOxDescription[4] = 'Due to a public holiday our offices will be closed on ' . date('d.m.Y',strtotime($feiertag['datum'])) . '. We will respond to your emails as soon as possible.';
        $arrOxDescription[5] = 'Jour férié!';
        $arrOxDescription[6] = 'En raison d\'un jour férié, nos bureaux seront fermés le ' . date('d.m.Y',strtotime($feiertag['datum'])) . '. Nous répondrons à vos courriels dans les plus brefs délais.';
        return $arrOxDescription;
    }

    public function executeSQL($arrOxDescription,$arrFromTo,$feiertag){
        $oDb = DatabaseProvider::getDb(DatabaseProvider::FETCH_MODE_ASSOC);

        $updateQueryNewsFeiertag = 'INSERT INTO bnsales_news(`OXID`, `OXSHORTDESC`, `OXLONGDESC`,`OXSHORTDESC_1`, `OXLONGDESC_1`,`OXSHORTDESC_2`, `OXLONGDESC_2`,`OXACTIVE`,`OXDATE`,`OXACTIVEFROM`,`OXACTIVETO`) VALUES (?,?,?,?,?,?,?,?,?,?,?)';
        $arrayQueryNewsFeiertag = array($arrOxDescription[0],
            $arrOxDescription[1],
            $arrOxDescription[2],
            $arrOxDescription[3],
            $arrOxDescription[4],
            $arrOxDescription[5],
            $arrOxDescription[6],
            0,
            date('Y-m-d',strtotime($feiertag['datum'])),
            $arrFromTo[0],
            $arrFromTo[1]
        );
        $oDb->execute($updateQueryNewsFeiertag,$arrayQueryNewsFeiertag);

        // Hier ordnen wir den Usern die Neuigkeiten zu in der Kreuztabelle oxuser2news..

        $arrUserSQL = 'SELECT OXID FROM oxuser WHERE OXACTIVE =1';
        $arrUser = $oDb->getAll($arrUserSQL);
        foreach($arrUser as $item){
            $updateQueryNewsUser = 'INSERT INTO oxuser2news(`OXID`,`OXSHOPID`,`OXUSERID`,`OXNEWSID`,`gelesen`) VALUES (?,?,?,?,?)';
            $oxidKreuzTabelle = 'Feiertag'.substr(\OxidEsales\Eshop\Core\UtilsObject::getInstance()->generateUID(),8);
            $OXSHOPID = 1;
            $oxuserid = $item['OXID'];
            $newsid = $arrOxDescription[0];
            $arrayKreuztabelle = array(
                $oxidKreuzTabelle,
                $OXSHOPID,
                $oxuserid,
                $newsid,
                0
            );
            $oDb->execute($updateQueryNewsUser,$arrayKreuztabelle);
        }
    }

    public function deleteSQL(){
        $oDb = DatabaseProvider::getDb(DatabaseProvider::FETCH_MODE_ASSOC);
        $query = 'SELECT * FROM bnsales_news WHERE OXID LIKE "Feiertag%" AND OXACTIVETO < CURRENT_TIMESTAMP';
        $result = $oDb->getAll($query);
        foreach($result as $key){
            $queryDeleteFromKreuz= 'DELETE FROM oxuser2news WHERE OXNEWSID = ?';
            $oDb->execute($queryDeleteFromKreuz,array($key['OXID']));
            $queryDeleteFromNews = 'DELETE FROM bnsales_news WHERE OXID = ?';
            $oDb->execute($queryDeleteFromNews,array($key['OXID']));
        }
    }
}