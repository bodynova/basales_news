<?php
#NICHT VERÄNDERN!!!!!!!!!!!
include('../../../../bootstrap.php');

use OxidEsales\Eshop\Core\DatabaseProvider;
$oDb = DatabaseProvider::getDb(DatabaseProvider::FETCH_MODE_ASSOC);

$cookie = $_COOKIE['sid'];
$name = ($_GET['name']);
$browser = ($_GET['browser']);
$getoxid = ($_GET['oxid']);
$oxid = array($getoxid);

try{
    $CookieQuery = 'SELECT CookieId FROM oxuser WHERE OXID = ?';
    $CookieResult = $oDb->getOne($CookieQuery,$oxid);
    if($CookieResult == $cookie){
        die('Kein neuer Cookie');
        return;
    } else {
        $CookieUpdate = 'UPDATE oxuser SET CookieId = ? WHERE OXID = ?';
        $arrCookie = array($cookie,$getoxid);
        $oDb->execute($CookieUpdate,$arrCookie);
    }
} catch(\Exception $e){
    echo 'ERROR: ' . $e->getMessage() . "\n";
}

try{
    $query = 'SELECT DeviceType FROM oxuser WHERE OXID = ?';
    $result = $oDb->getOne($query,$oxid);
    //die($result == null);
} catch(\Exception $e){
    echo 'Error: ' . $e->getMessage() . "\n";
}

$bool = false;
if($result != null){
    $arrDevice = json_decode($result,true);
    $geraete = $arrDevice['Geraete'];
    $count = 0;
    foreach($geraete as $key){
        if($name == $key['Name'] && $browser == $key['BName']){
            $arrDevice['Geraete'][$count]['Aufrufe'] +=1;
            $bool = true;
        }
        $count++;
    }
    if(!$bool){
        array_push($arrDevice['Geraete'],array('Name'=>$name,'BName' => $browser,'Aufrufe' => 1));
    }
    print_r($arrDevice);
    $sDevice = json_encode($arrDevice);
} else {
    $sDevice = '{"Geraete":[{"Name":"'.$name.'","BName":"'.$browser.'","Aufrufe":1}]}';
}

try{
    $arrUpdate = array($sDevice,$getoxid);
    $query = 'UPDATE oxuser SET DeviceType =? WHERE OXID = ?';
    $oDb->execute($query,$arrUpdate);
}catch(\Exception $e){
    echo $e->getMessage() . "ERROR";
}

echo $sDevice;
//die('Neues Gerät');

/*foreach($asd as $key){
    echo $key;
}*/