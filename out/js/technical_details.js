$(document).ready(function(){
    $('.editable').each(function(){
        this.contentEditable = true;
    });
});
function activeFromTo(x,y){

    //console.log(x);
    //console.log(y);
    if(x.checked){
        document.getElementById('title' + y).setAttribute('required',true);
    } else {
        document.getElementById('title' + y).removeAttribute('required');
    }

    if(document.getElementById('selectBox').checked === true || document.getElementById('selectBox_1').checked === true || document.getElementById('selectBox_2').checked === true){
        document.getElementById('dateFrom').setAttribute('disabled',true);
        document.getElementById('dateTo').setAttribute('disabled',true);
    } else {
        document.getElementById('dateFrom').removeAttribute('disabled');
        document.getElementById('dateTo').removeAttribute('disabled');
    }
}


function checkHTML(x,y){

    var doc = document.createElement('div');
    doc.innerHTML = x;
    if(!(doc.innerHTML === x) ){
        var r = confirm('Es gibt einen HTML-Fehler. Soll der Text automatisch validiert werden?');
        if(r === true){
            document.getElementById(y).value = doc.innerHTML;
        }
    }

    /*for(var i = 0;i<doc.innerHTML.length;i++){
        if(doc.innerHTML[i] !== x[i]){
            //console.log(doc.innerHTML[i]);
        }
    }*/
    //TODO: Eventuell bei altem Text belassen.
}

/**
 *
 * @param x NewsID
 * @param lang language
 */
function editNews(x,lang){
    //console.log(x);
    $('#editNewsModal'+x).modal({backdrop: 'static', keyboard: false});
    if(lang=== "de"){
        $('#modalDE'+x).show();
        $('#modalEN'+x).hide();
        $('#modalFR'+x).hide();
        $('#dexy'+x)[0].style.height = "";$('#dexy'+x)[0].style.height = $('#dexy'+x)[0].scrollHeight + "px";
    } else if(lang==="en"){
        $('#modalDE'+x).hide();
        $('#modalEN'+x).show();
        $('#modalFR'+x).hide();
        $('#enxy'+x)[0].style.height = "";$('#enxy'+x)[0].style.height = $('#enxy'+x)[0].scrollHeight + "px";
    } else if(lang==="fr"){
        $('#modalDE'+x).hide();
        $('#modalEN'+x).hide();
        $('#modalFR'+x).show();
        $('#frxy'+x)[0].style.height = "";$('#frxy'+x)[0].style.height = $('#frxy'+x)[0].scrollHeight + "px";
    }

}

/**
 *
 * @param x News-ID
 * @param lang aktuelle Sprache
 */
function showText(x,lang){

    var content = null;
    if(x === null){
        content = $('#content'+lang)[0].value;
        $('#previewBody').empty();
        $('#preview').modal({backdrop: 'static', keyboard: false});
        $('#previewBody').append(content);
        return;
    } else {
        if(lang === 'de'){
            content = $('#dexy'+x)[0].value;
        } else if(lang ==='en'){
            content = $('#enxy'+x)[0].value;
        } else if(lang === 'fr'){
            content = $('#frxy'+x)[0].value;
        }
    }
    console.log(content);
    $('#previewBody').empty();
    $('#preview').modal({backdrop: 'static', keyboard: false});
    $('#previewBody').append(content);

    $('.editable').empty();
    $('.editable').append(content);
    $('#editNewsModal'+x).fadeOut(300);
    $('#dismiss').attr('x',x);
}

function closeVorschau(){
    if($('#dismiss')[0].attributes[5]){
        var modal = $('#dismiss')[0].attributes[5].value;
        $('#editNewsModal'+modal).fadeIn(300);
    }
}

/**
 *
 * @param x News-ID
 * @param lang aktuelle Sprache
 */
function isEmpty(x,lang){
    //console.log(lang);
    if(lang === 'de'){
        $('#dexy'+x)[0].value = $('#despan'+x)[0].innerHTML;
    } else if(lang ==='en'){
        $('#enxy'+x)[0].value = $('#enspan'+x)[0].innerHTML;
    } else if(lang === 'fr'){
        $('#frxy'+x)[0].value = $('#frspan'+x)[0].innerHTML;
    }
}

function proofKey(e,x){
    console.log(x);
    console.log(e.key);
    var temporalDivElement = document.createElement("div");
    temporalDivElement.innerHTML = e.key;



    if(e.key === "Enter"){
        alert('Für Zeilenumbrüche bitte oben den HTML-Button benutzen.');
        $('.idUmbruch').fadeOut(300).fadeIn(300).fadeOut(300).fadeIn(300);
        e.preventDefault();
    } else if(e.key === "Backspace" || e.key === "Delete"){
        document.getElementById("sel").value = "";
    } else if((e.key === "<" || e.key === ">" || e.key === "&")/*&& !x.id.includes("content")*/) {
        replaceSelectedText(document.getElementById(x.id),temporalDivElement.innerHTML);
        e.preventDefault();
    }
}


function ptag(x,lang){
    event.preventDefault();
    //console.log(document.getElementById("sel"));
    var l = null;
    if(lang === undefined){
        var contentLang = $(x).attr("content");
        el = document.getElementById('content' + contentLang);
    } else {
        if(lang === 'de'){
            el = document.getElementById("dexy"+x);
        } else if(lang ==='en'){
            el = document.getElementById("enxy"+x);
        } else if(lang === 'fr'){
            el = document.getElementById("frxy"+x);
        }
    }


    var sel = getInputSelection(el);


    replaceSelectedText(el, '<p>' + document.getElementById("sel").value + '</p>');
    document.getElementById("sel").value = "";
    el.selectionEnd = sel.end+3;
    el.focus();



}

function fett(x,lang){
    event.preventDefault();
    //console.log(document.getElementById("sel"));
    var l = null;
    if(lang === undefined){
        var contentLang = $(x).attr("content");
        el = document.getElementById('content' + contentLang);
    } else {
        if(lang === 'de'){
            el = document.getElementById("dexy"+x);
        } else if(lang ==='en'){
            el = document.getElementById("enxy"+x);
        } else if(lang === 'fr'){
            el = document.getElementById("frxy"+x);
        }
    }
    var sel = getInputSelection(el);
    replaceSelectedText(el, '<b>' + document.getElementById("sel").value + '</b>');
    document.getElementById("sel").value = "";
    el.selectionEnd = sel.end+3;
    el.focus();
}

function underline(x,lang){
    event.preventDefault();
    //console.log(document.getElementById("sel"));
    var l = null;
    if(lang === undefined){
        var contentLang = $(x).attr("content");
        el = document.getElementById('content' + contentLang);
    } else {
        if(lang === 'de'){
            el = document.getElementById("dexy"+x);
        } else if(lang ==='en'){
            el = document.getElementById("enxy"+x);
        } else if(lang === 'fr'){
            el = document.getElementById("frxy"+x);
        }
    }
    var sel = getInputSelection(el);
    replaceSelectedText(el, '<u>' + document.getElementById("sel").value + '</u>');
    document.getElementById("sel").value = "";
    el.selectionEnd = sel.end+3;
    el.focus();
}

function kursiv(x,lang){
    event.preventDefault();
    //console.log(document.getElementById("sel"));
    var l = null;
    if(lang === undefined){
        var contentLang = $(x).attr("content");
        el = document.getElementById('content' + contentLang);
    } else {
        if(lang === 'de'){
            el = document.getElementById("dexy"+x);
        } else if(lang ==='en'){
            el = document.getElementById("enxy"+x);
        } else if(lang === 'fr'){
            el = document.getElementById("frxy"+x);
        }
    }
    var sel = getInputSelection(el);
    replaceSelectedText(el, '<i>' + document.getElementById("sel").value + '</i>');
    document.getElementById("sel").value = "";
    el.selectionEnd = sel.end+3;
    el.focus();
}

function umbruch(x,lang){


    event.preventDefault();
    //console.log(document.getElementById("sel"));
    var l = null;
    if(lang === undefined){
        var contentLang = $(x).attr("content");
        el = document.getElementById('content' + contentLang);
    } else {
        if(lang === 'de'){
            el = document.getElementById("dexy"+x);
        } else if(lang ==='en'){
            el = document.getElementById("enxy"+x);
        } else if(lang === 'fr'){
            el = document.getElementById("frxy"+x);
        }
    }
    console.log(el);

    var sel = getInputSelection(el);
    replaceSelectedText(el, '<br>');
    document.getElementById("sel").value = "";
    el.selectionEnd = sel.end+4;
    el.focus();
}

function testAbschnitt(){
    var replacementText = document.getElementById('sel').value;
    replacementText = '<p>' + replacementText + '</p>';
    testDiv(replacementText);
}
function testFett(){
    var replacementText = document.getElementById('sel').value;
    replacementText = '<b>' + replacementText + '</b>';
    testDiv(replacementText);
}
function testKursiv(){
    var replacementText = document.getElementById('sel').value;
    replacementText = '<i>' + replacementText + '</i>';
    testDiv(replacementText);
}
function testUnder(){
    var replacementText = document.getElementById('sel').value;
    replacementText = '<u>' + replacementText + '</u>';
    testDiv(replacementText);
}
function testUmbruch(){
    var replacementText = document.getElementById('sel').value;
    replacementText = '<br>';
    testDiv(replacementText);
}

function testDiv(replacementText) {

    var sel, range;
    //$('#dexyFeiertag9c2135f79667df0b27977d35').append(replacementText);
    if (window.getSelection) {
        sel = window.getSelection();
        if (sel.rangeCount) {
            range = sel.getRangeAt(0);
            range.deleteContents();
            range.insertNode(document.createTextNode(replacementText));
        }
    } else if (document.selection && document.selection.createRange) {
        range = document.selection.createRange();
        range.text = replacementText;
    }
    console.log($('#testDiv')[0].innerHTML);
    console.log($('#dexyFeiertag9c2135f79667df0b27977d35')[0].innerHTML = $('#testDiv')[0].innerHTML);
    $('#previewButton').click();

}

function getInputSelection(el) {
    var start = 0, end = 0, normalizedValue, range,
        textInputRange, len, endRange;
    if (typeof el.selectionStart == "number" && typeof el.selectionEnd == "number") {
        start = el.selectionStart;
        end = el.selectionEnd;
    } else {
        range = document.selection.createRange();

        if (range && range.parentElement() == el) {
            len = el.value.length;
            normalizedValue = el.value.replace(/\r\n/g, "\n");

            // Create a working TextRange that lives only in the input
            textInputRange = el.createTextRange();
            textInputRange.moveToBookmark(range.getBookmark());

            // Check if the start and end of the selection are at the very end
            // of the input, since moveStart/moveEnd doesn't return what we want
            // in those cases
            endRange = el.createTextRange();
            endRange.collapse(false);

            if (textInputRange.compareEndPoints("StartToEnd", endRange) > -1) {
                start = end = len;
            } else {
                start = -textInputRange.moveStart("character", -len);
                start += normalizedValue.slice(0, start).split("\n").length - 1;

                if (textInputRange.compareEndPoints("EndToEnd", endRange) > -1) {
                    end = len;
                } else {
                    end = -textInputRange.moveEnd("character", -len);
                    end += normalizedValue.slice(0, end).split("\n").length - 1;
                }
            }
        }
    }

    return {
        start: start,
        end: end
    };
}

function replaceSelectedText(el, text) {
    var sel = getInputSelection(el), val = el.value;
    //console.log(sel);
    el.value = val.slice(0, sel.start) + text + val.slice(sel.end);
}

function getSelectionText() {
    var text = "";
    var activeEl = document.activeElement;
    var activeElTagName = activeEl ? activeEl.tagName.toLowerCase() : null;
    if (
        (activeElTagName == "textarea") || (activeElTagName == "input" &&
        /^(?:text|search|password|tel|url)$/i.test(activeEl.type)) &&
        (typeof activeEl.selectionStart == "number")
    ) {
        text = activeEl.value.slice(activeEl.selectionStart, activeEl.selectionEnd);
    } else if (window.getSelection) {
        text = window.getSelection().toString();
    }
    if(text === ""){
        return document.getElementById("sel").value;
    } else {
        return text;
    }
}

document.onmouseup = document.onkeyup = document.onselectionchange = function() {
    document.getElementById("sel").value = getSelectionText();
};


/**
 *
 * @param x ID
 * @param lang Sprache
 */
function changeLangModal(x, lang) {

    if (lang === "de") {
        $('#modalDE'+x).show();
        $('#modalEN'+x).hide();
        $('#modalFR'+x).hide();
        $('input[name="lang"]').val('')
        //TODO: Lang Input
        //console.log($('#dexy' + x));
        $('#dexy' + x)[0].style.height = "";
        $('#dexy' + x)[0].style.height = $('#dexy' + x)[0].scrollHeight + "px";
    } else if (lang === "en") {
        $('#modalDE'+x).hide();
        $('#modalEN'+x).show();
        $('#modalFR'+x).hide();

        $('input[name="lang"]').val('_1')
        $('#enxy' + x)[0].style.height = "";
        $('#enxy' + x)[0].style.height = $('#enxy' + x)[0].scrollHeight + "px";
    } else if (lang === "fr") {
        $('#modalDE'+x).hide();
        $('#modalEN'+x).hide();
        $('#modalFR'+x).show();

        $('input[name="lang"]').val('_2')
        $('#frxy' + x)[0].style.height = "";
        $('#frxy' + x)[0].style.height = $('#frxy' + x)[0].scrollHeight + "px";
    }

    $('#previewButton').attr('lang', lang);
    $('#abortButton').attr('lang', lang);
}

/**
 *
 * @param lang Sprache
 */
function changeLang(lang){
    if(lang=== "de"){
        $('.bigDE').each(function(){
            $(this).show();
        });
        $('.bigEN').each(function(){
            $(this).hide();
        });
        $('.bigFR').each(function(){
            $(this).hide();
        });
       /* $('#BigDE').show();
        $('#BigEN').hide();
        $('#BigFR').hide();*/
        //$('#langInput').val('de');
        $('input[name="lang"]').val('')
    } else if(lang==="en"){
        $('.bigDE').each(function(){
            $(this).hide();
        });
        $('.bigEN').each(function(){
            $(this).show();
        });
        $('.bigFR').each(function(){
            $(this).hide();
        });
        /*$('#BigDE').hide();
        $('#BigEN').show();
        $('#BigFR').hide(); */
        //$('#langInput').val('en');
        $('input[name="lang"]').val('_1')
    } else if(lang==="fr"){
        $('.bigDE').each(function(){
            $(this).hide();
        });
        $('.bigEN').each(function(){
            $(this).hide();
        });
        $('.bigFR').each(function(){
            $(this).show();
        });
        /*$('#BigDE').hide();
        $('#BigEN').hide();
        $('#BigFR').show();*/
        //$('#langInput').val('fr');
        $('input[name="lang"]').val('_2')
    }
    $('#previewButton').attr('lang', lang);
    $('#abortButton').attr('lang', lang);
}

/**
 *
 * @param lang Sprache
 */
function changeButtonContent(lang){
    $('#buttonBar .btn').each(function(){
       var button = $(this);
       $(this).show();
       $(this).attr("content",lang);
       //console.log(button);
    });
}
