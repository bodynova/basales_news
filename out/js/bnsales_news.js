

function showModal(x) {
    //

    var res = $.ajax({
        type: "GET",
        url: '/index.php?cl=bnsales_mynews&fnc=showLists&lang=' + x,
        async: false,
        dataType: "html",
        error: function (){
            handleAjaxLogout(res)
        },
        success: function (res) {
            //
            $('body').append(res);
            //
            if($('#bodyNews')[0].firstChild.length === 16){
                $('#showNews').attr('data-original-title','Keine weiteren neuen Nachrichten!');
                return;
            }
            //console.log($('#bodyNews')[0].firstChild.length);

            $('#userNews').modal();
            //
            //
            $('#userNews').on('hide.bs.modal', function (e) {
                $('#userNews').remove();
            });
        }
    });

    // ende
}

function showAnzahl(x){
    var res = $.ajax({
        type: "GET",
        url: '/index.php?cl=bnsales_mynews&fnc=showAnzahl&lang='+x,
        async: false,
        dataType: "html",
        error: function (){
            handleAjaxLogout(res)
        },
        success: function (res) {
            $('#badgeAnzahl').text(res);
        }
    });
}

function next(oxid,x){

    //console.log(x);
    var res = $.ajax({
        type: "GET",
        url: '/index.php?cl=bnsales_mynews&fnc=getNext&oxid=' + oxid,
        async: false,
        dataType: "html",
        error: function (){
            handleAjaxLogout(res)
        },
        success: function (res) {
            //console.log(oxid);

            //$('#modalClose').click();
            $('#userNews').modal('hide');
            showModal(x);
            //console.log($('body'));
            //$('#userNews').remove();
            showAnzahl(x);
        }
    });
    if($('#bodyNews')[0].firstChild.length !== 16){
        setTimeout(function(){$("body").addClass("modal-open");},250);
    } else {
        //location.reload();
        $('#showNews').remove();
        $('#oldNewsTag').removeAttr('style');
        //console.log($('#showNews'));
    }

}

/*
function back(){
    var res = $.ajax({
        type: "GET",
        url: '/index.php?cl=bnsales_mynews&fnc=getPrev',
        async: false,
        dataType: "html",
        error: function (){
            handleAjaxLogout(res)
        },
        success: function (res) {
            console.log(res);
            //$('#modalClose').click();
            $('#userNews').modal('hide');
            showModal();
            showAnzahl();
        }
    });
}
*/

function commentEmail(){
    var oxid = $('#headNews')[0].innerText; // Titel
    var vorname = $('input[name="vorname"]')[0].value;
    var nachname = $('input[name="nachname"]')[0].value;
    var email = $('input[name="userEmail"]')[0].value;
    var comment = $('#commentEmailTA')[0].value;
    var firma  = $('input[name="firma"]')[0].value;
    var x = null;

    if(comment === ""){
        x = 0;
    } else {
        x = 1;
    }

    //console.log(comment);
    //console.log(oxid+vorname+nachname+email+firma);
    var res = $.ajax({
        type: "GET",
        url: '/index.php?cl=bnsales_mynews&fnc=commentEmail&oxid='+oxid+'&vorname='+vorname+'&nachname='+nachname+'&email='+email+'&comment='+comment+'&firma='+firma,
        async: false,
        error: function (){
            handleAjaxLogout(res)
        },
        success: function (res) {
            var inhaltModal= "";
            //console.log(res);
            if(!x){
                if($('#abortComment').length > 0){
                    //console.log($('#abortComment'));
                    $('#abortComment').fadeOut(300).fadeIn(300).fadeOut(300).fadeIn(300);
                } else {
                    inhaltModal = '<div id="abortComment" class="card text-white bg-warning mb-3">' +
                                    '<div class="card-body">' +
                                    res +
                                    '</div>' +
                                    '</div>';
                    $('#formEmail').append(inhaltModal);
                }
                //setTimeout(function(){$('#formEmail').append(inhaltModal);},350);
                //$('#abortComment').remove();
            } else {
                inhaltModal = '<div class="card text-white bg-success mb-3">' +
                                '<div class="card-body">' +
                                res +
                                '</div>' +
                                '</div>';
                $('#formEmail').empty();
            $('#formEmail').append(inhaltModal);
            }
        }
    });
}

function activeFromTo(x,y){

    //console.log(x);
    //console.log(y);
    if(x.checked){
        document.getElementById('title' + y).setAttribute('required',true);
    } else {
        document.getElementById('title' + y).removeAttribute('required');
    }

    if(document.getElementById('selectBox').checked === true || document.getElementById('selectBox_1').checked === true || document.getElementById('selectBox_2').checked === true){
        document.getElementById('dateFrom').setAttribute('disabled',true);
        document.getElementById('dateTo').setAttribute('disabled',true);
    } else {
        document.getElementById('dateFrom').removeAttribute('disabled');
        document.getElementById('dateTo').removeAttribute('disabled');
    }
}

function openOldNews(x,oxid,user,all = undefined){

    var Box = $('#newsBox' + x);
    var Fa = $('#newsFa' + x);
    var Span = $('#newsSpan' + x);
    var SpanChild = $('#newsSpanChild' + x);
    if($('#bluePoint' +x).length == 1){

        $('#bluePoint' +x).remove();
        //Span.attr('style','');
        SpanChild.attr('style','margin-left:10px;');
        if(all === undefined){
            var res = $.ajax({
                type: "POST",
                url: '/index.php?cl=bnsales_oldnews&fnc=updateGelesen',
                data:{
                    user: user,
                    oxid: oxid
                },
                error: function (){

                },
                success: function (res) {
                }
            });
        }

        //console.log('fett');
    }

    if(Box.attr('style') === 'display:none'){
        Box.attr('style','display:block');
        Fa.removeClass();
        Fa.addClass('fa fa-arrow-down');
    } else if(Box.attr('style') === 'display:block'){
        Box.attr('style','display:none');
        Fa.removeClass();
        Fa.addClass('fa fa-arrow-right');
    }
}

function checkHTML(x,y){
    console.log(x);
    console.log(y);
    var doc = document.createElement('div');
    doc.innerHTML = x;
    if(!(doc.innerHTML === x) ){
        var r = confirm('Es gibt einen HTML-Fehler. Soll der Text automatisch validiert werden?');
        if(r === true){
            document.getElementById(y).value = doc.innerHTML;
        }
    }
    for(var i = 0;i<doc.innerHTML.length;i++){
        if(doc.innerHTML[i] !== x[i]){
            //console.log(doc.innerHTML[i]);
        }
    }
    //TODO: Eventuell bei altem Text belassen.
}

/**
 * Funktion: Öffnet alle News (jede News wird geklickt)
 * @param x thisButton
 * @param user OXUSER-ID
 */
function openAllNews(x,user){

    if(x.children[0].className === 'fa fa-expand'){
        x.children[0].className = 'fa fa-compress';
    } else {
        x.children[0].className = 'fa fa-expand';
    }
    //user = '"' + user +'"';
    var stringOxid = '(';
    $('.openNews').each(function(){
        //console.log($(this));
        //console.log($(this));
        stringOxid +='"'+$(this)[0].attributes[1].nodeValue + '",';
        x= $(this)[0].id.substring(8);

        var Box = $('#newsBox' + x);
        var Fa = $('#newsFa' + x);

        if(Box.attr('style') === 'display:none'){
            Box.attr('style','display:block');
            Fa.removeClass();
            Fa.addClass('fa fa-arrow-down');
        } else if(Box.attr('style') === 'display:block'){
            Box.attr('style','display:none');
            Fa.removeClass();
            Fa.addClass('fa fa-arrow-right');
        }
        //$(this).click();
    });
    stringOxid = stringOxid.substr(0,stringOxid.length-1);
    stringOxid += ')';
    var res = $.ajax({
        type: "POST",
        url: '/index.php?cl=bnsales_oldnews&fnc=updateGelesenAll',
        data:{
            user: user,
            oxid: stringOxid
        },
        error: function (){

        },
        success: function (res) {
        }
    });

}

/**
 *
 * @param x NewsID
 */
function editNews(x){
    $('#editNewsModal'+x).modal();
    $('#xy'+x)[0].style.height = "";$('#xy'+x)[0].style.height = $('#xy'+x)[0].scrollHeight + "px";
}

/**
 * TODO: Text-Vorschau HTML
 */
function showText(){

}
