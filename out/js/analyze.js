
function analyze(x){
    $.ajax({
        type: "GET",
        url: '/index.php?cl=analyze&fnc=getData',
        error: function (res){
            console.log('ERROR');
        },
        success: function (res) {
           //console.log(res);
        }
    });
    //console.log(x);
}

document.addEventListener('DOMContentLoaded', function () {
    var myChart = Highcharts.chart('container', {
        chart: {
            type: 'pie',
        },
        title: {
            text: 'Gerätetypen'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            series: {
                events:  {


                }
            },
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,

                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                }
            }
        },
        navigation: {
            buttonOptions: {
                enabled: false
            }
        },
        series: [{
            name: 'Anteil',
            colorByPoint: true,
            data: [1,1,1]
        }]
    });
});
