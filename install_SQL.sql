CREATE TABLE `bnsales_news`  (
  `OXID` char(32) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL COMMENT 'News id',
  `OXSHOPID` int(11) NOT NULL DEFAULT 1 COMMENT 'Shop id (oxshops)',
  `OXACTIVE` tinyint(1) NOT NULL DEFAULT 1 COMMENT 'Is active',
  `OXACTIVEFROM` datetime(0) NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'Active from specified date',
  `OXACTIVETO` datetime(0) NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'Active to specified date',
  `OXDATE` date NOT NULL DEFAULT '0000-00-00' COMMENT 'Creation date (entered by user)',
  `OXSHORTDESC` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT 'Short description (multilanguage)',
  `OXLONGDESC` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'Long description (multilanguage)',
  `OXACTIVE_1` tinyint(1) NOT NULL DEFAULT 0,
  `OXSHORTDESC_1` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `OXLONGDESC_1` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `OXACTIVE_2` tinyint(1) NOT NULL DEFAULT 0,
  `OXSHORTDESC_2` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `OXLONGDESC_2` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `OXACTIVE_3` tinyint(1) NOT NULL DEFAULT 0,
  `OXSHORTDESC_3` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `OXLONGDESC_3` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `OXTIMESTAMP` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Timestamp',
  PRIMARY KEY (`OXID`)
);
CREATE TABLE `oxuser2news`  (
  `OXID` char(32) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL COMMENT 'Record id',
  `OXSHOPID` char(32) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL DEFAULT '' COMMENT 'Shop id (oxshops)',
  `OXUSERID` char(32) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL DEFAULT '' COMMENT 'User id',
  `OXNEWSID` char(32) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL DEFAULT '' COMMENT 'Group id',
  `OXTIMESTAMP` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT 'Timestamp',
  `gelesen` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`OXID`)
);

UPDATE TABLE `oxuser` (
    `DeviceType` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
    `CookieId` CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL
)