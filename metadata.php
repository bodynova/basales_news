<?php
/**
 * Metadata version
 */
$sMetadataVersion = '2.0';


/**
 * Module information
 */
$aModule = [
    'id'           => 'bnsales_news',
    'title'        => '<img src="../modules/bodynova/bnsales_news/out/img/favicon.ico" title="Bodynova Haendlershop Functions">odynova HaendlerShop News',
    'description'  => [
        'de' => 'Modul zur Bereitstellung der News Funktionen',
        'en' => 'Module for sales shop news functions'
    ],
    'thumbnail'     => 'out/img/logo_bodynova.png',
    'version'      => '2.0.0',
    'author'       => 'André Bender',
    'email'        => 'a.bender@bodynova.de',
    'controllers'  => [
        /* vorlage */
        /*
        'dre_user_rights'  =>
            \Bender\dre_AdminRights\Application\Controller\Admin\dre_user_rights::class
        */

        'technicaldetails' =>
            \Bodynova\bnSales_News\Application\Controller\technicaldetails::class,

        'bnsales_mynews' =>
            \Bodynova\bnSales_News\Application\Controller\bnsales_mynews::class,
        'bnsales_oldnews' =>
            \Bodynova\bnSales_News\Application\Controller\bnsales_oldnews::class,
        'bnsales_mainuser' =>
            \Bodynova\bnSales_News\Application\Controller\bnsales_mainuser::class,
        'analyze' =>
            \Bodynova\bnSales_News\Application\Controller\analyze::class
    ],
    'extend'       => [

        /* \OxidEsales\Eshop\Application\Controller\FrontendController::class      =>
            \Bodynova\bnSales_News\Application\Controller\bnsales_mynews::class */
        /* vorlage */
        /*
        \OxidEsales\Eshop\Application\Model\Article::class                      =>
            \Bender\dre_AdminRights\Application\Model\dre_article::class,
        */
        \OxidEsales\Eshop\Core\Model\MultiLanguageModel::class                      =>
            \Bodynova\bnSales_News\Application\Model\bnsales_news::class,
        \OxidEsales\Eshop\Application\Model\User::class =>
            \Bodynova\bnSales_News\Application\Model\bnsales_User::class,
        \OxidEsales\Eshop\Application\Model\Article::class =>
            \Bodynova\bnSales_News\Application\Model\bnsales_article::class,
        \OxidEsales\Eshop\Application\Controller\Admin\UserMain::class =>
            \Bodynova\bnSales_News\Application\Controller\bnsales_mainuser::class,
        \Bodynova\bnSales_News\Application\Controller\bnsales_mainuser::class =>
        \Bodynova\bnSales_News\Application\Controller\analyze::class



    ],
    'templates' => [
        'bnsales_user.tpl' => 'bodynova/bnsales_news/Application/views/tpl/bnsales_user.tpl',
        'article_technical_details.tpl' => 'bodynova/bnsales_news/Application/views/tpl/article_technical_details.tpl',
        /* vorlage */
        /*
        'dre_user_rights.tpl'  =>
            'bender/dre_adminrights/Application/views/admin/tpl/dre_user_rights.tpl',
        */
        'bnsales_mynews.tpl'    =>
            'bodynova/bnsales_news/Application/views/tpl/bnsales_mynews.tpl',
        'bnsales_oldnews.tpl'    =>
            'bodynova/bnsales_news/Application/views/tpl/bnsales_oldnews.tpl',
        'analyze.tpl'    =>
            'bodynova/bnsales_news/Application/views/tpl/analyze.tpl',
    ],
    'settings' => [
        [
            'group' => 'bnsales_main',
            'name' => 'email_news',
            'type' => 'str',
            'value' => 'server@bodynova.de'
        ],
        [

            'group' => 'bnsales_feiertage',
            'name' => 'Feiertage_NRW',
            'type' => 'bool',
            'value' => 'false'
        ],
        [
            'group' => 'bnsales_feiertage',
            'name' => 'Feiertage_BW',
            'type' => 'bool',
            'value' => 'false'
        ],
        [
            'group' => 'bnsales_feiertage',
            'name' => 'zeitspanne_feiertag',
            'type' => 'str',
            'value' => '10'
        ],
        [
            'group' => 'bnsales_feiertage',
            'name' => 'urlFeiertage',
            'type' => 'str',
            'value' => 'https://feiertage-api.de/api/?jahr=2020&nur_land=NW'
        ],
        [
            'group' => 'bnsales_editnews',
            'name' => 'bnsales_oxgroup',
            'type' => 'select',
            'value' => 'alle',
            'constraints' => 'alle|Gruppe_1|Gruppe_2|Gruppe_3'
        ],
        [
            'group' => 'bnsales_editnews',
            'name' => 'bnsales_oxactive',
            'type' => 'bool',
            'value' => 'false'
        ],
        [
            'group' => 'bnsales_editnews',
            'name' => 'bnsales_oxdate',
            'type' => 'str',
            'value' => '0000-00-00'
        ],
        [
            'group' => 'bnsales_editnews',
            'name' => 'bnsales_oxactivefrom',
            'type' => 'str',
            'value' => '0000-00-00'
        ],
        [
            'group' => 'bnsales_editnews',
            'name' => 'bnsales_oxactiveto',
            'type' => 'str',
            'value' => '0000-00-00'
        ],
        [
            'group' => 'bnsales_editnews',
            'name' => 'bnsales_oxshortdesc',
            'type' => 'str',
        ],
        [
            'group' => 'bnsales_editnews',
            'name' => 'bnsales_oxlongdesc',
            'type' => 'arr',
        ]

    ],
    'events' => [
        /*
        'onActivate' =>
            'Bender\dre_AdminRights\Core\Events::onActivate'
        */
    ],
    'blocks'      => [
        array(
            'template' => 'widget/footer/services.tpl',
            'block' => 'footer_newslink',
            'file' => 'Application/views/blocks/tpl/widget/footer/dre_services.tpl'
        ),
        array(
            'template' => 'layout/base.tpl',
            'block' => 'base_js',
            'file' => 'Application/views/blocks/tpl/layout/bnsales_base_js.tpl'
        ),
        array(
            'template' => 'layout/base.tpl',
            'block' => 'base_style',
            'file' => 'Application/views/blocks/tpl/layout/bnsales_base_css.tpl'
        ),
    ]
];

?>
